#ifndef VERSION_H
#define VERSION_H

namespace AutoVersion{
	
	//Software Status
	static const char STATUS[] =  "Beta";
	static const char STATUS_SHORT[] =  "b";
	
	//Standard Version Type
	static const long MAJOR  = 0;
	static const long MINOR  = 8;
	static const long BUILD  = 92;
	static const long REVISION  = 373;
	
	//Miscellaneous Version Types
	static const long BUILDS_COUNT  = 4119;
	#define RC_FILEVERSION 0,8,92,373
	#define RC_FILEVERSION_STRING "0, 8, 92, 373\0"
	static const char FULLVERSION_STRING [] = "0.8.92.373";
	
	//These values are to keep track of your versioning state, don't modify them.
	static const long BUILD_HISTORY  = 108;
	

}
#endif //VERSION_H
