/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "logger.h"

#include "coms.h"

Logger::Logger()
{
    running = false;
}

bool Logger::begin(std::string path)
{
    if(running)
        return false;

    of.open(path);
    if(!of.is_open())
        return false;

    std::stringstream now;

    auto tp = std::chrono::system_clock::now();
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>( tp.time_since_epoch() );
    size_t modulo = ms.count() % 1000;

    time_t seconds = std::chrono::duration_cast<std::chrono::seconds>( ms ).count();

    char buffer[25];

    if(strftime(buffer, 25, "%Y-%m-%d %H:%M:%S.", localtime(&seconds)))
        now << buffer;

    now.fill('0');
    now.width(3);
    now << modulo;

    of << "<FlightLog>\n";
    of << "\t<DateTime>" << now.str() << "</DateTime>\n";
    of.flush();

    thr = std::thread(&Logger::logger_thread, this);
    return true;
}

void Logger::end()
{
    if(running)
    {
        running = false;
        thr.join();
        of << "</FlightLog>\n";
        of.flush();
        of.close();
    }
}

bool Logger::is_logging()
{
    return running;
}

/*
 * Some notes about this function...
 * msg_vec is also accessed by the logger_thread, but only the coms class needs to call this function.
 * Calling coms->lock() and coms->unlock() in the logger_thread should prevent any change of a race condition
 */
void Logger::log_message(std::string msg)
{
    if(running)
        msg_vec.push_back(msg);
}

void Logger::logger_thread()
{
    Coms* coms = Coms::instance();
    msg_vec.clear();
    running = true;
    while(running)
    {
        mute.lock();
        bool ofopen = of.is_open();
        mute.unlock();

        if(!ofopen)
        {
            running = false;
            cout << "logger thread exiting" << endl;
            return;
        }

        coms->lock();

        std::stringstream ss;
        ss.precision(2);
        ss << std::fixed << coms->get_volts() << ", ";
        ss << coms->get_timer() << ", ";
        ss << coms->get_amps() << ", ";
        ss << coms->get_temperature() << ", ";
        ss << int(coms->get_power_remaining()) << ", ";
        ss << coms->get_motor_armed() << ", ";
        ss << coms->get_autothrottle_on() << ", ";
        ss << 0 << ", ";
        ss << coms->get_ground_track() << ", ";
        ss << coms->get_heading() << ", ";
        ss << coms->get_pitch() << ", ";
        ss << coms->get_roll() << ", ";
        ss << coms->get_alt() << ", ";
        ss << coms->get_vert_speed() << ", ";
        ss << coms->get_air_speed() << ", ";
        ss.precision(6);
        ss << coms->get_latitude() << ", ";
        ss << coms->get_longitude() << ", ";
        ss.precision(2);
        ss << coms->get_ground_speed() << ", ";
        ss << int(coms->get_satellites_used()) << ", ";

        ss << int(coms->get_gps_mode()) << ", ";
        ss << int(coms->get_ap_mode()) << ", ";
        ss << coms->get_ap_pitch() << ", ";
        ss << coms->get_ap_roll() << ", ";
        ss << coms->get_ap_heading() << ", ";
        ss << coms->get_ap_alt() << ", ";
        ss.precision(6);
        ss << coms->get_ap_waypoint_lat() << ", ";
        ss << coms->get_ap_waypoint_lng() << ", ";
        ss.precision(2);
        ss << coms->get_ap_waypoint();
        ss.flush();

        coms->unlock();

        mute.lock();
        of << "\t<Entry>\n\t\t<Stream>";
        of << ss.str();
        of << "</Stream>\n";

        for(int i = 0; i < msg_vec.size(); i++)
        {
            of << "\t\t<Pkt>";

            std::string msg;
            for(int j = 0; j < msg_vec[i].size(); j++)
            {
                std::stringstream hss;
                hss << std::hex << std::setfill('0');
                hss << std::setw(2) << uint32_t(msg_vec[i][j]);
                msg.append(hss.str(), hss.str().length()-2, 2);
                msg.append(" ");
            }

            of << msg << "</Pkt>\n";
        }
        msg_vec.clear();

        of << "\t</Entry>\n";
        of.flush();
        mute.unlock();
        sleep_ms(100);
    }
}
