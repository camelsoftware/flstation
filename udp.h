/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef UDP_H_INCLUDED
#define UDP_H_INCLUDED

//#include "utility.h"

#include <stdint.h>

#ifndef _WIN32

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>

#else

#include <winsock2.h>
#include <stdio.h>

#endif

class UDPSocket
{
	public:
        UDPSocket();

		bool listen(int port);

		bool set_timeout(int us);
		bool set_write_address(const char* hostname, uint16_t port);

		int read(void* buf, int maxlen);

		int write(void* buf, int len);

	protected:
		int sock;
		struct sockaddr_in listen_addr, write_addr;
};

#endif
