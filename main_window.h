/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MAIN_WINDOW_H_INCLUDED
#define MAIN_WINDOW_H_INCLUDED

#include <gtkmm.h>
#include "coms.h"
#include "flight_display.h"
#include "planner.h"
#include "connect.h"
#include "configuration.h"
#include "logwindow.h"
#include "radio.h"
#include "term.h"
#include "about.h"



class mainWindow_da : public Gtk::DrawingArea
{
    public:
        mainWindow_da(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
        virtual ~mainWindow_da();
    protected:
        bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

};



class mainWindow : public Gtk::Window
{
public:
    mainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~mainWindow();


private:

    void on_connect_button_clicked();
    void on_fly_button_clicked();
    void on_plan_button_clicked();
    void on_configure_button_clicked();
    void on_logs_button_clicked();
    void on_radio_button_clicked();
    void on_terminal_button_clicked();
    void on_about_button_clicked();

    bool timer(int tn);


    Glib::RefPtr<Gtk::Builder> refBuilder;

    FlightDisplay* flightdisplay;
    Planner* planner;
    Config* config;
    RadioWindow* radio;
    Terminal* terminal;
    LogWindow* logwindow;

    Gtk::Button* connect;
    Gtk::Button* plan;
    Gtk::Button* fly;
    Gtk::Button* configure;
    Gtk::Button* logs;
    Gtk::Button* radio_btn;
    Gtk::Button* term;
    Gtk::Button* about;

    Gtk::Label* connected_lbl;
    Gtk::Label* con_strength_lbl;

    Gtk::Label* connect_lbl;

    mainWindow_da* da;
};




#endif // MAIN_WINDOW_H_INCLUDED
