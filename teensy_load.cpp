/* Teensy Loader, Command Line Interface
 * Program and Reboot Teensy Board with HalfKay Bootloader
 * http://www.pjrc.com/teensy/loader_cli.html
 * Copyright 2008-2010, PJRC.COM, LLC
 *
 * Adapted by Samuel Cowen for the Firetail UAV Project
 *
 * You may redistribute this program and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software
 * Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/
 */

/* Want to incorporate this code into a proprietary application??
 * Just email paul@pjrc.com to ask.  Usually it's not a problem,
 * but you do need to ask to use this code in any way other than
 * those permitted by the GNU General Public License, version 3  */

/* For non-root permissions on ubuntu or similar udev-based linux
 * http://www.pjrc.com/teensy/49-teensy.rules
 */

#include "teensy_load.h"
#include <thread>
#include <iostream>

using namespace std;

int teensy_progress = 0;
const int teensy_code_size = 262144;
const int teensy_block_size = 1024;
std::thread teensy_fmw_thr;

bool teensy_thr_running = false;


void teensy_reset()
{
    teensy_progress = 0;
}

void teensy_load_thread()
{
    teensy_thr_running = true;
    unsigned char buf[2048];
    int addr, r, write_size=teensy_block_size+2;
    int first_block=1;


    for (addr = 0; addr < teensy_code_size; addr += teensy_block_size)
    {
        teensy_progress = ((double)addr / (double)teensy_code_size) * 100.0;
        if (!first_block && !teensy_ihex_bytes_within_range(addr, addr + teensy_block_size - 1))
        {
            // don't waste time on blocks that are unused,
            // but always do the first one to erase the chip
            continue;
        }
        if (!first_block && teensy_memory_is_blank(addr, teensy_block_size)) continue;
        if (teensy_code_size < 0x10000)
        {
            buf[0] = addr & 255;
            buf[1] = (addr >> 8) & 255;
            teensy_ihex_get_data(addr, teensy_block_size, buf + 2);
            write_size = teensy_block_size + 2;
        }
        else if (teensy_block_size == 256)
        {
            buf[0] = (addr >> 8) & 255;
            buf[1] = (addr >> 16) & 255;
            teensy_ihex_get_data(addr, teensy_block_size, buf + 2);
            write_size = teensy_block_size + 2;
        }
        else if (teensy_block_size >= 1024)
        {
            buf[0] = addr & 255;
            buf[1] = (addr >> 8) & 255;
            buf[2] = (addr >> 16) & 255;
            memset(buf + 3, 0, 61);
            teensy_ihex_get_data(addr, teensy_block_size, buf + 64);
            write_size = teensy_block_size + 64;
        }
        else
        {
            teensy_progress = -2;
            teensy_thr_running = false;
            return;
        }
        r = teensy_write(buf, write_size, first_block ? 3.0 : 0.25);
        if (!r)
        {
            teensy_progress = -3;
            teensy_thr_running = false;
            return;
        }
        first_block = 0;
    }

    buf[0] = 0xFF;
    buf[1] = 0xFF;
    buf[2] = 0xFF;
    memset(buf + 3, 0, sizeof(buf) - 3);
    teensy_write(buf, write_size, 0.25);

    teensy_close();
    teensy_progress = 100.0;
    teensy_thr_running = false;
}


int teensy_load_progress()
{
    return teensy_progress;
}


bool teensy_load_start(Gtk::Window* wd, std::string fname)
{
    if(teensy_thr_running)
        return false;

    int num = teensy_read_intel_hex(fname.c_str());
    cout << num << endl;
    if(num < 0)
    {
        Gtk::MessageDialog msg(*wd, "Failed to open file.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The file could not be opened.");
        cout << fname << endl;
        msg.run();
        return false;
    }

    if(!teensy_open())
    {
        Gtk::MessageDialog msg(*wd, "Could not open Teensy!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("Is the autopilot connected?");
        msg.run();
        return false;
    }

    if(teensy_fmw_thr.joinable())
        teensy_fmw_thr.join();
    teensy_fmw_thr = std::thread(teensy_load_thread);
    return true;
}




/****************************************************************/
/*                                                              */
/*             USB Access - libusb (Linux & FreeBSD)            */
/*                                                              */
/****************************************************************/

#ifndef _WIN32

// http://libusb.sourceforge.net/doc/index.html
#include <usb.h>

#else

#include <lusb0_usb.h>

#endif

usb_dev_handle * open_usb_device(int vid, int pid)
{
    struct usb_bus *bus;
    struct usb_device *dev;
    usb_dev_handle *h;
    char buf[128];
    int r;

    usb_init();
    usb_find_busses();
    usb_find_devices();
    for (bus = usb_get_busses(); bus; bus = bus->next)
    {
        for (dev = bus->devices; dev; dev = dev->next)
        {
            if (dev->descriptor.idVendor != vid) continue;
            if (dev->descriptor.idProduct != pid) continue;
            h = usb_open(dev);
            if (!h)
            {
                continue;
            }
#ifdef LIBUSB_HAS_GET_DRIVER_NP
            r = usb_get_driver_np(h, 0, buf, sizeof(buf));
            if (r >= 0)
            {
                r = usb_detach_kernel_driver_np(h, 0);
                if (r < 0)
                {
                    usb_close(h);
                    continue;
                }
            }
#endif
            // Mac OS-X - removing this call to usb_claim_interface() might allow
            // this to work, even though it is a clear misuse of the libusb API.
            // normally Apple's IOKit should be used on Mac OS-X
            r = usb_claim_interface(h, 0);
            if (r < 0)
            {
                usb_close(h);
                continue;
            }
            return h;
        }
    }
    return NULL;
}

static usb_dev_handle *libusb_teensy_handle = NULL;

int teensy_open(void)
{
    teensy_close();
    libusb_teensy_handle = open_usb_device(0x16C0, 0x0478);
    if (libusb_teensy_handle) return 1;
    return 0;
}

int teensy_write(void *buf, int len, double timeout)
{
    int r;

    if (!libusb_teensy_handle) return 0;
    while (timeout > 0)
    {
        r = usb_control_msg(libusb_teensy_handle, 0x21, 9, 0x0200, 0,
                            (char *)buf, len, (int)(timeout * 1000.0));
        if (r >= 0) return 1;
        usleep(10000);
        timeout -= 0.01;  // TODO: subtract actual elapsed time
    }
    return 0;
}

void teensy_close(void)
{
    if (!libusb_teensy_handle) return;
    usb_release_interface(libusb_teensy_handle, 0);
    usb_close(libusb_teensy_handle);
    libusb_teensy_handle = NULL;
}

int hard_reboot(void)
{
    usb_dev_handle *rebootor;
    int r;

    rebootor = open_usb_device(0x16C0, 0x0477);
    if (!rebootor) return 0;
    r = usb_control_msg(rebootor, 0x21, 9, 0x0200, 0, "reboot", 6, 100);
    usb_release_interface(rebootor, 0);
    usb_close(rebootor);
    if (r < 0) return 0;
    return 1;
}


/****************************************************************/
/*                                                              */
/*                     Read Intel Hex File                      */
/*                                                              */
/****************************************************************/

// the maximum flash image size we can support
// chips with larger memory may be used, but only this
// much intel-hex data can be loaded into memory!
#define MAX_MEMORY_SIZE 0x80000

static unsigned char firmware_image[MAX_MEMORY_SIZE];
static unsigned char firmware_mask[MAX_MEMORY_SIZE];
static int end_record_seen=0;
static int byte_count;
static unsigned int extended_addr = 0;
static int teensy_parse_hex_line(char *line);

int teensy_read_intel_hex(const char *filename)
{
    FILE *fp;
    int i, lineno=0;
    char buf[1024];

    byte_count = 0;
    end_record_seen = 0;
    for (i=0; i<MAX_MEMORY_SIZE; i++)
    {
        firmware_image[i] = 0xFF;
        firmware_mask[i] = 0;
    }
    extended_addr = 0;

    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        //printf("Unable to read file %s\n", filename);
        return -1;
    }
    while (!feof(fp))
    {
        *buf = '\0';
        if (!fgets(buf, sizeof(buf), fp)) break;
        lineno++;
        if (*buf)
        {
            if (teensy_parse_hex_line(buf) == 0)
            {
                return -2;
            }
        }
        if (end_record_seen) break;
        if (feof(stdin)) break;
    }
    fclose(fp);
    return byte_count;
}

int teensy_parse_hex_line(char *line)
{
    int addr, code, num;
    int sum, len, cksum, i;
    char *ptr;

    num = 0;
    if (line[0] != ':') return 0;
    if (strlen(line) < 11) return 0;
    ptr = line+1;
    if (!sscanf(ptr, "%02x", &len)) return 0;
    ptr += 2;
    if ((int)strlen(line) < (11 + (len * 2)) ) return 0;
    if (!sscanf(ptr, "%04x", &addr)) return 0;
    ptr += 4;
    /* printf("Line: length=%d Addr=%d\n", len, addr); */
    if (!sscanf(ptr, "%02x", &code)) return 0;
    if (addr + extended_addr + len >= MAX_MEMORY_SIZE)  { cout << "max memory size" << endl; return 0; }
    ptr += 2;
    sum = (len & 255) + ((addr >> 8) & 255) + (addr & 255) + (code & 255);
    if (code != 0)
    {
        if (code == 1)
        {
            end_record_seen = 1;
            return 1;
        }
        if (code == 2 && len == 2)
        {
            if (!sscanf(ptr, "%04x", &i)) return 1;
            ptr += 4;
            sum += ((i >> 8) & 255) + (i & 255);
            if (!sscanf(ptr, "%02x", &cksum)) return 1;
            if (((sum & 255) + (cksum & 255)) & 255) return 1;
            extended_addr = i << 4;
            //printf("ext addr = %05X\n", extended_addr);
        }
        if (code == 4 && len == 2)
        {
            if (!sscanf(ptr, "%04x", &i)) return 1;
            ptr += 4;
            sum += ((i >> 8) & 255) + (i & 255);
            if (!sscanf(ptr, "%02x", &cksum)) return 1;
            if (((sum & 255) + (cksum & 255)) & 255) return 1;
            extended_addr = i << 16;
            //printf("ext addr = %08X\n", extended_addr);
        }
        return 1;	// non-data line
    }
    byte_count += len;
    while (num != len)
    {
        if (sscanf(ptr, "%02x", &i) != 1)  { cout << "sscanf failed" << endl; return 0; }
        i &= 255;
        firmware_image[addr + extended_addr + num] = i;
        firmware_mask[addr + extended_addr + num] = 1;
        ptr += 2;
        sum += i;
        (num)++;
        if (num >= 256) { cout << "num >= 256" << endl; return 0; }
    }
    if (!sscanf(ptr, "%02x", &cksum))  { cout << "sscanf failed here" << endl; return 0; }
    if (((sum & 255) + (cksum & 255)) & 255)  { cout << "checksum error" << endl; return 0; } /* checksum error */
    return 1;
}

int teensy_ihex_bytes_within_range(int begin, int end)
{
    int i;

    if (begin < 0 || begin >= MAX_MEMORY_SIZE ||
            end < 0 || end >= MAX_MEMORY_SIZE)
    {
        return 0;
    }
    for (i=begin; i<=end; i++)
    {
        if (firmware_mask[i]) return 1;
    }
    return 0;
}

void teensy_ihex_get_data(int addr, int len, unsigned char *bytes)
{
    int i;

    if (addr < 0 || len < 0 || addr + len >= MAX_MEMORY_SIZE)
    {
        for (i=0; i<len; i++)
        {
            bytes[i] = 255;
        }
        return;
    }
    for (i=0; i<len; i++)
    {
        if (firmware_mask[addr])
        {
            bytes[i] = firmware_image[addr];
        }
        else
        {
            bytes[i] = 255;
        }
        addr++;
    }
}

int teensy_memory_is_blank(int addr, int block_size)
{
    if (addr < 0 || addr > MAX_MEMORY_SIZE) return 1;

    while (block_size && addr < MAX_MEMORY_SIZE)
    {
        if (firmware_mask[addr] && firmware_image[addr] != 255) return 0;
        addr++;
        block_size--;
    }
    return 1;
}

