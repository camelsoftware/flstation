/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FLIGHT_DISPLAY_H_INCLUDED
#define FLIGHT_DISPLAY_H_INCLUDED

#include "adi.h"
#include "hsi.h"
//#include "mfd.h"
#include "logger.h"
#include "refresh_waypoints.h"
#include "satellites.h"
#include "alarms_window.h"
#include "sound_window.h"
#include "steering.h"


class baseCoordinatesDialog : public Gtk::Dialog
{
public:
    baseCoordinatesDialog();
    Gtk::Entry* lat_entry, *lng_entry, *ele_entry;
};



class magVarDialog : public Gtk::Dialog
{
public:
    magVarDialog();
    Gtk::Entry* magvar_entry;
};


class batCapDialog : public Gtk::Dialog
{
public:
    batCapDialog();
    Gtk::Entry* cap_entry;
    Gtk::SpinButton* pc_spin;
};

class mapTypeDialog : public Gtk::Dialog
{
public:
    mapTypeDialog();
    Gtk::ComboBoxText* map_types;
};


class FlightDisplay : public Gtk::Window
{
public:
    FlightDisplay(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~FlightDisplay();

    void redraw();

    void show();
    bool on_delete_event(GdkEventAny* ent);

private:

    void manual_toggled();
    void trainer_toggled();
    void pnr_toggled();
    void hna_toggled();
    void loiter_toggled();
    void rtb_toggled();
    void waypoints_toggled();

    void arm_motor_switched();
    void autothrottle_switched();
    void autolaunch_clicked();
    void logging_toggled();

    void on_next_waypoint();
    void on_back_waypoint();
    void on_restart_flight();

    void on_get_base_pos();
    void on_set_base_pos();
    void on_mag_var_clicked();
    void on_change_map_type();
    void on_refresh_waypoints();

    void on_hil_clicked();
    void on_set_bat_cap_clicked();
    void on_sound_options_clicked();


    void clear_messages();
    bool check_message_txt(std::string msg);

    void alert(std::string msg);

    Glib::RefPtr<Gtk::Builder> refBuilder;

    RefreshWaypointsWindow* rwp_window;
    SatellitesWindow* satellites_window;
    AlarmsWindow* alarms_window;
    SoundWindow* sound_window;
    SteeringWindow* steering_window;

    Gtk::RadioButton* mode_manual;
    Gtk::RadioButton* mode_trainer;
    Gtk::RadioButton* mode_pnr;
    Gtk::RadioButton* mode_hna;
    Gtk::RadioButton* mode_loiter;
    Gtk::RadioButton* mode_rtb;
    Gtk::RadioButton* mode_waypoints;

    Gtk::Switch* motor_switch;

    Gtk::CheckMenuItem* autothrottle_item;

    ADI* adi;
    HSI* hsi;
    //MFD* mfd;

    Gtk::Label* groundspeed_lbl;
    Gtk::Label* alt_agl_lbl;
    Gtk::Label* gps_lock_lbl;
    Gtk::Label* rtb_dist_lbl;
    Gtk::Label* timer_lbl;
    Gtk::Label* temp_lbl;
    Gtk::Label* rc_rx_ok_lbl;
    Gtk::Label* inside_fence_lbl;
    Gtk::Label* wp_dist_lbl;

    Gtk::ProgressBar* power_progress;
    Gtk::Label* current_label;
    Gtk::Label* volts_label;

    Gtk::Button* get_base_pos_button;
    Gtk::Button* set_base_pos_button;
    Gtk::Button* mag_var_button;

    Gtk::Button* hil_mode_button;

    Gtk::ToggleButton* logging_toggle;
    Gtk::ToggleButton* lock_on_toggle;

    class ModelColumns : public Gtk::TreeModelColumnRecord
    {
    public:

        ModelColumns()
        {
            add(m_txt);
        }

        Gtk::TreeModelColumn<std::string> m_txt;
    };

    Gtk::TreeView* msg_tree;
    ModelColumns m_Columns;
    Glib::RefPtr<Gtk::ListStore> refmsglist;

    shared_ptr<Logger> logger;

    int last_ap_waypoint;
    int timeoutctr;

    int alt_warn_counter;
    const int alt_warn_threshold = 50;
};


#endif // FLIGHT_DISPLAY_H_INCLUDED
