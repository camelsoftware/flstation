/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TEENSY_LOADER_INCLUDED
#define TEENSY_LOADER_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <gtkmm.h>
#include "utility.h"


int teensy_open(void);
int teensy_write(void *buf, int len, double timeout);
void teensy_close(void);
int teensy_hard_reboot(void);
int teensy_read_intel_hex(const char *filename);
int teensy_ihex_bytes_within_range(int begin, int end);
void teensy_ihex_get_data(int addr, int len, unsigned char *bytes);
int teensy_memory_is_blank(int addr, int block_size);


bool teensy_load_start(Gtk::Window* wd, std::string fname);
int teensy_load_progress();
void teensy_reset();

#endif // TEENSY_LOADER_INCLUDED
