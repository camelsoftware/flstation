/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "alarms_window.h"

#include <iostream>
using namespace std;

AlarmsWindow::AlarmsWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget("alarms_treeview", treeview);

    m_refTreeModel = Gtk::ListStore::create(m_Columns);
    treeview->set_model(m_refTreeModel);

    treeview->append_column("Alarms", m_Columns.text);

    Gtk::Button* btn;
    refBuilder->get_widget("alarms_clear_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &AlarmsWindow::clear));

    ctr = 0;
}

AlarmsWindow::~AlarmsWindow()
{

}

void AlarmsWindow::timeout()
{
    Coms* coms = Coms::instance();
    ctr++;
    if(ctr > 2)
    {
        ctr = 0;
        coms->lock();
        if(coms->get_n_alarms())
        {
            Gtk::TreeModel::Row row = *(m_refTreeModel->append());
            row[m_Columns.text] = coms->get_next_alarm();
        }
        coms->unlock();
    }
}

void AlarmsWindow::clear()
{
    m_refTreeModel->clear();
}
