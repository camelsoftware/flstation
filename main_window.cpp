/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "main_window.h"

mainWindow_da::mainWindow_da(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder)
    : Gtk::DrawingArea(cobject)
{

}

mainWindow_da::~mainWindow_da()
{

}

bool mainWindow_da::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    std::string imagepath;
    imagepath = share_uri;
    imagepath += "flstation.png";

    Cairo::RefPtr<Cairo::ImageSurface> r_image = Cairo::ImageSurface::create_from_png(imagepath);
    Cairo::RefPtr<Cairo::ImageSurface> new_image = Cairo::ImageSurface::create(r_image->get_format(), width, height);
    Cairo::RefPtr<Cairo::Context> icr = Cairo::Context::create(new_image);

    icr->scale(width/1024.0, height/642.0);
    icr->set_source(r_image, 0, 0);
    icr->rectangle(0, 0, 1024.0, 642.0);
    icr->fill();

    cr->set_source(new_image, 0, 0);
    cr->rectangle((width/2)-(new_image->get_width()/2), (height/2)-(new_image->get_height()/2),
                  width, height);
    cr->fill();

    return true;
}



mainWindow::mainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget("connect_button", connect);
    refBuilder->get_widget("fly_button", fly);
    refBuilder->get_widget("plan_button", plan);
    refBuilder->get_widget("configure_button", configure);
    refBuilder->get_widget("logs_button", logs);
    refBuilder->get_widget("radios_button", radio_btn);
    refBuilder->get_widget("terminal_button", term);
    refBuilder->get_widget("about_button", about);

    refBuilder->get_widget("connected_lbl", connected_lbl);
    refBuilder->get_widget("connection_strength_lbl", con_strength_lbl);

    refBuilder->get_widget("connect_label", connect_lbl);

    connect->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_connect_button_clicked));
    fly->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_fly_button_clicked));
    plan->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_plan_button_clicked));
    configure->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_configure_button_clicked));
    logs->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_logs_button_clicked));
    radio_btn->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_radio_button_clicked));
    term->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_terminal_button_clicked));
    about->signal_clicked().connect(sigc::mem_fun(*this, &mainWindow::on_about_button_clicked));

    refBuilder->get_widget_derived("flight_window", flightdisplay);
    refBuilder->get_widget_derived("planner_window", planner);
    refBuilder->get_widget_derived("config_window", config);
    refBuilder->get_widget_derived("terminal_window", terminal);
    refBuilder->get_widget_derived("log_window", logwindow);
    refBuilder->get_widget_derived("radio_window", radio);

    refBuilder->get_widget_derived("main_drawing_area", da);

    sigc::slot<bool> slot = sigc::bind(sigc::mem_fun(*this, &mainWindow::timer), 0);
    sigc::connection conn = Glib::signal_timeout().connect(slot, 50);

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);

    set_title("FLStation");

    maximize();
}


mainWindow::~mainWindow()
{
    Coms::instance()->stop();
}


void mainWindow::on_connect_button_clicked()
{
    Coms* coms = Coms::instance();
    if(coms->running())
    {
        coms->stop();
        connect_lbl->set_text("Connect");
    }
    else
    {
        connectDlg connect_dlg;
        if(connect_dlg.run() == Gtk::RESPONSE_OK)
        {
            if(connect_dlg.page == 0)
            {
                coms->address = connect_dlg.coms_port;
                coms->baudport = connect_dlg.baud;
                coms->m = COMS_SERIAL;
            }
            if(connect_dlg.page == 1)
            {
                coms->address = connect_dlg.udp_addr;
                coms->baudport = connect_dlg.udp_port;
                coms->m = COMS_UDP;
            }

            coms->start();

            //pause while thread begins to run
            sleep_ms(100);
            if(coms->running())
            {
                connect_lbl->set_text("Disconnect");
            }
            else
            {
                Gtk::MessageDialog dialog(*this, "Couldn't open coms port!",
                                          false, Gtk::MESSAGE_INFO,
                                          Gtk::BUTTONS_OK);
                dialog.set_secondary_text(
                    "Please check that your radio is connected.");
                dialog.run();
            }
        }
    }

}

void mainWindow::on_fly_button_clicked()
{
    flightdisplay->show();
}


void mainWindow::on_plan_button_clicked()
{
    planner->show();
}

void mainWindow::on_configure_button_clicked()
{
    config->show();
}

void mainWindow::on_radio_button_clicked()
{
    radio->show();
}

void mainWindow::on_terminal_button_clicked()
{
    terminal->show();
}

void mainWindow::on_about_button_clicked()
{
    AboutDlg aboutdlg;
    aboutdlg.run();
}

void mainWindow::on_logs_button_clicked()
{
    Coms* coms = Coms::instance();
    if(coms->running())
    {
        Gtk::MessageDialog dialog(*this, "Disconnect first!",
                                    false, Gtk::MESSAGE_INFO,
                                    Gtk::BUTTONS_OK);
        dialog.set_secondary_text(
                    "You can't replay a log while the Coms system is running. Please disconnect and try again.");
        dialog.run();
        return;
    }
    logwindow->show();
}

bool mainWindow::timer(int n)
{
    char txt[100];
    Coms* coms = Coms::instance();
    sprintf(txt, "Link Quality: %i pps", coms->connection_strength());
    con_strength_lbl->set_text(txt);

    if((coms->m == COMS_LOG_PLAYBACK) && (coms->running()))
    {
        connect->set_sensitive(false);
        connect_lbl->set_text("Replaying log");
    }
    else
    {
        connect->set_sensitive(true);
        if(coms->running())
        {
            connected_lbl->set_text("Connected");
            connect_lbl->set_text("Disconnect");
        }
        else
        {
            connected_lbl->set_text("Disconnected");
            connect_lbl->set_text("Connect");
        }
    }

    if(flightdisplay->is_visible())
        flightdisplay->redraw();
    if(config->is_visible())
        config->timeout();
    if(planner->is_visible())
        planner->timeout();
    if(terminal->is_visible())
        terminal->timeout();

    return true;
}



