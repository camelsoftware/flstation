/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "offlinehsi.h"


OfflineHSI::OfflineHSI(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::DrawingArea(cobject)
{
    zoom_level = 1.0;

    add_events(Gdk::BUTTON_PRESS_MASK);
    signal_button_press_event().connect( sigc::mem_fun( *this, &OfflineHSI::onMouseDown ) );
}

OfflineHSI::~OfflineHSI()
{

}


void OfflineHSI::redraw()
{
    Glib::RefPtr<Gdk::Window> win = get_window();
    if(win)
    {
        win->invalidate(false);
    }
}

bool OfflineHSI::onMouseDown(GdkEventButton* _eve)
{
    Gdk::Event eve((GdkEvent*)_eve);
    double x, y;
    eve.get_coords(x, y);

    if((x >= 5) && (x <= 15))
    {
        if((y >= 5) && (y <= 15))
        {
            zoom_level /= 2;
            if(zoom_level < 0.125)
                zoom_level = 0.125;
        }

        if((y >= 16) && (y <= 26))
        {
            zoom_level *= 2;
            if(zoom_level >= 64.0)
                zoom_level = 64.0;
        }
    }
    return false;
}


bool OfflineHSI::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    Coms* coms = Coms::instance();

    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);
    cr->rectangle(0, 0, width, height);
    cr->fill();


    cr->set_source_rgba(0.8, 0.8, 0.8, 1.0);
    cr->rectangle(5, 5, 10, 10);
    cr->fill();

    cr->set_source_rgba(0.8, 0.8, 0.8, 1.0);
    cr->rectangle(5, 16, 10, 10);
    cr->fill();

    cr->set_source_rgba(0.2, 0.2, 0.2, 1.0);
    cr->move_to(10, 6);
    cr->line_to(10, 14);
    cr->stroke();

    cr->move_to(6, 10);
    cr->line_to(14, 10);
    cr->stroke();

    cr->move_to(6, 21);
    cr->line_to(14, 21);
    cr->stroke();


    float dia;
    if(height < width)
        dia = height/2;
    else
        dia = width/2;

    float hheight = height/2;
    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->arc(width/2, height/2, dia/2, 0, M_PI*2);
    cr->stroke();

    int angle = 30;
    while(angle < 370)
    {
        float adj = cos(((angle-90))*(M_PI/180.0))*(dia/2);
        float opp = sin((M_PI/180.0)*((angle-90)))*(dia/2);

        float adj2 = cos((M_PI/180)*((angle-90)))*((dia/2)+10);
        float opp2 = sin((M_PI/180)*((angle-90)))*((dia/2)+10);

        float adj3 = cos((M_PI/180)*((angle-90)))*((dia/2)+20);
        float opp3 = sin((M_PI/180)*((angle-90)))*((dia/2)+20);

        cr->move_to(width/2+adj, hheight+opp);
        cr->line_to(width/2+adj2, hheight+opp2);
        cr->stroke();

        char buf[100];
        cr->move_to(width/2+adj3-9, hheight+opp3+5);
        sprintf(buf, "%i", angle);
        cr->show_text(buf);
        cr->stroke();
        angle += 30;
    }


    //draw the course made good indication
    coms->lock();
    float track = coms->get_ground_track(); //get the track
    coms->unlock();

    track -= 90; //rotate anticlockwise 90 degrees to that north points up on the screen
    track *= M_PI/180; //convert to radians

    int cmg_x, cmg_y; //calculate points for the line from the diameter and track angle
    cr->set_source_rgb(0.0, 0.0, 0.8); //make it blue
    cr->move_to(width/2, height/2);
    cmg_y = sin(track)*(dia/2);
    cmg_x = cos(track)*(dia/2);
    cr->line_to((width/2)+cmg_x, (height/2)+cmg_y);
    cr->stroke(); //draw the track line

    cr->move_to(10, height-60);
    std::stringstream cmgss;
    cmgss.precision(2);
    cmgss << "COURSE:" << int(coms->get_ground_track()+0.5);
    cr->show_text(cmgss.str());



    //calculate distance to home base using equirectangular approximation (pythagoras' theorem)
    float hb_d = distance_between(make_coordf(coms->get_latitude(), coms->get_longitude()),
                                  make_coordf(coms->home_lat, coms->home_lng));

    cr->set_source_rgba(0.7, 0.2, 0.2, 1.0);

    if(hb_d != 0) //if distance is zero, we won't bother drawing it
    {
        //calculate great circle bearing to home base
        float hb_brng = to_radians(bearing_from_to(make_coordf(coms->get_latitude(), coms->get_longitude()),
                                        make_coordf(coms->home_lat, coms->home_lng)));

        float opp = sin(hb_brng-(M_PI/2)) * (hb_d*(1/zoom_level));
        float adj = cos(hb_brng-(M_PI/2)) * (hb_d*(1/zoom_level));

        cr->move_to(width/2, height/2);
        cr->line_to((width/2)+adj, (height/2)+opp);
        cr->stroke();
    }

    cr->move_to(10, height-30);
    std::stringstream ss_dist;
    ss_dist << "RTB: " << int(hb_d+0.5) << "m";
    cr->show_text(ss_dist.str().c_str());
    cr->stroke();



    float head = coms->get_heading() * (M_PI/180.0);
    cr->translate(width/2, height/2);
    cr->rotate(head);

    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->move_to(0, 15);
    cr->line_to(0, -15);
    cr->stroke();

    cr->move_to(-20, 0);
    cr->line_to(20, 0);
    cr->stroke();

    cr->move_to(-7, 15);
    cr->line_to(7, 15);
    cr->stroke();


    cr->rotate(-head);
    cr->translate(-width/2, -height/2);


    //draw the scale bar
    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->move_to(width-110, height - 10);
    cr->line_to(width-10, height - 10);
    cr->stroke();

    std::stringstream ss;
    ss << int(zoom_level*100) << "m";

    cr->move_to(width-110, height-15);
    cr->show_text(ss.str().c_str());
    cr->stroke();


    if(coms->connection_strength() < 10)
    {
        cr->set_source_rgba(1.0, 0.0, 0.0, 1.0);
        cr->move_to(0, 0);
        cr->line_to(width, height);
        cr->stroke();

        cr->move_to(width, 0);
        cr->line_to(0, height);
        cr->stroke();
    }
}
