#ifndef STEERING_WINDOW_H
#define STEERING_WINDOW_H

#include <gtkmm.h>
#include "coms.h"

class SteeringWindow : public Gtk::Window
{
public:
    SteeringWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~SteeringWindow();

private:

};


#endif // STEERING_WINDOW_H
