/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef REFRESH_WAYPOINTS_H
#define REFRESH_WAYPOINTS_H

#include <thread>
#include <mutex>
#include <gtkmm.h>
#include "coms.h"

class RefreshWaypointsWindow : public Gtk::Window
{
public:
    RefreshWaypointsWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~RefreshWaypointsWindow();

    void show();
    bool on_delete_event(GdkEventAny* event);

    void timeout();

private:
    Glib::RefPtr<Gtk::Builder> refBuilder;

    void on_cancel_clicked();
    void thr_func();

    Gtk::Label* lbl;
    Gtk::ProgressBar* prog;
    Gtk::Button* cancel_btn;

    std::thread thr;
    std::mutex mute;

    bool stop_thread; //thread only reads this

    int status;
    int n_points;
    float pc; //thread only writes this

};


#endif // REFRESH_WAYPOINTS_H
