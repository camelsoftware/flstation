#include "globals.h"
#include <iostream>
#include <fstream>

using namespace std;

global_fields globals;

void global_fields_init()
{
    #ifdef _WIN32
    sprintf(globals.coms_port, "COM1");
    sprintf(globals.terminal_port, "COM1");
    sprintf(globals.radio_port, "COM1");
    #else
    sprintf(globals.coms_port, "/dev/ttyUSB0");
    sprintf(globals.terminal_port, "/dev/ttyACM0");
    sprintf(globals.radio_port, "/dev/ttyUSB0");
    #endif

    sprintf(globals.coms_addr, "localhost");

    globals.coms_baud = 57600;
    globals.terminal_baud = 115200;
    globals.coms_udp_port = 5684;
    globals.snd_mute = false;
    globals.snd_disconnect_vol = 100;
    globals.snd_alarm_vol = 100;
    globals.snd_buzzer_vol = 100;

    std::string fname = Glib::get_home_dir();
    fname += "/.fls_rc";

    std::ifstream fs;
    fs.open(fname);
    if(!fs.is_open())
    {
        cout << "Couldn't open settings file! Using default settings instead." << endl;
        return;
    }
    fs.read((char*)&globals, sizeof(globals));
    fs.close();
}

void global_fields_write()
{
    std::string fname = Glib::get_home_dir();
    fname += "/.fls_rc";


    std::ofstream fs;
    fs.open(fname);
    if(!fs.is_open())
    {
        cout << "Error saving settings to " << fname << endl;
        return;
    }

    fs.write((char*)&globals, sizeof(globals));
    fs.close();
}

