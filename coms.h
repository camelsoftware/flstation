/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COMS_H_INCLUDED
#define COMS_H_INCLUDED

#include <fstream>
#include <mutex>
#include <string>
#include <ctime>
#include <memory>
#include <vector>

#include "logger.h"
#include "serial/serial.h"
#include "udp.h"
#include "FireLink/firelink.h"
#include "utility.h"

#include <iostream>

using namespace std;


enum Coms_Method
{
    COMS_SERIAL,
    COMS_UDP,
    COMS_LOG_PLAYBACK
};

//a singleton! shock horror!
class Coms : public Firelink::Station, public Camel::Thread
{
    public:
        static Coms* instance();
        void lock();
        void unlock();

        bool start();
        void stop(); //function to stop thread. blocks until thread exits

        bool running();

        int connection_strength();

        bool wait_for_dispatch(uint16_t pid);
        bool wait_for_message(uint16_t pid);

        void push_log_pack(std::string pk);

        float get_heading() { return inst.heading/100.0; }
        float get_pitch() { return inst.pitch/100.0; }
        float get_roll() { return inst.roll/100.0; }
        float get_ap_pitch() { return ap_status.pitch/100.0; }
        float get_ap_roll() { return ap_status.roll/100.0; }
        float get_ap_heading() { return ap_status.heading/100.0; }
        float get_alt() { return inst.baro_alt; }
        float get_vert_speed() { return inst.vert_speed; }
        float get_ap_alt() { return ap_status.alt; }
        float get_air_speed() { return inst.air_speed; }
        float get_latitude() { return inst.latitude/1000000.0; }
        float get_longitude() { return inst.longitude/1000000.0; }
        int get_satellites_used() { return systems.sats_used; }
        Firelink::AP_MODE get_ap_mode() { return ap_status.mode; }
        bool get_autothrottle_on();
        bool get_autolaunch_on();
        bool get_motor_armed();
        bool get_has_rc_signal();
        bool get_is_inside_geofence();
        Firelink::vs_raw_marg get_raw_marg() { return marg; }
        Firelink::vs_raw_pwm get_raw_pwm() { return pwm; }
        int get_ap_waypoint() { return ap_status.wp_number; }
        float get_ground_speed() { return systems.ground_speed/100.0; }
        float get_ground_track() { return systems.ground_track/100.0; }
        Firelink::GPS_LOCK get_gps_mode() { return systems.gps_lock; }
        uint32_t get_timer() { return systems.timer; }
        float get_temperature() { return systems.temperature/100.0; }
        int get_pwm_inputs(int c);
        float get_ap_waypoint_lat() { return ap_status.wp_lat/1000000.0; }
        float get_ap_waypoint_lng() { return ap_status.wp_lng/1000000.0; }
        int get_power_remaining() { return systems.power_remaining; }
        float get_amps() { return systems.milliamps/1000.0; }
        float get_volts() { return systems.volts/100.0; }

        Firelink::m_satellites sat_reports[12]; //a place to store received satellite reports

        void set_logger(std::shared_ptr<Logger> l) { logger = l; }

        float home_lat, home_lng, home_alt;

        std::string address;
        int baudport;
        Coms_Method m;

        void ThreadFunc();

        //returns number of alerts that have been received
        int get_n_alarms();
        //returns the next alert
        std::string get_next_alarm();

        std::vector<waypoint> plan_points;
        std::vector<waypoint> fence_points;

    private:
        Coms();
        ~Coms();
        Coms& operator=(Coms const&){ return *this; };

        Firelink::vs_inst inst;
        Firelink::vs_systems systems;
        Firelink::vs_ap_status ap_status;
        Firelink::vs_raw_marg marg;
        Firelink::vs_raw_pwm pwm;

        bool on_recv_satellites(uint8_t* data);

        bool on_recv_home_coordinates(uint8_t* data);

        bool on_recv_alarms(uint8_t* data);

        bool on_recv_n_points(uint8_t* data);
        bool on_recv_point(uint8_t* data);

        bool on_recv_debug_msg(uint8_t* data);

        std::shared_ptr<Logger> logger;
        bool serial();
        bool udp();

        Camel::Mutex mute;

        serial::Serial sp;
        UDPSocket* sock;

        const int keep_alive_seconds = 3;
        time_t keep_alive_time; //time of last keep-alive
        int pack_count = 0;
        int last_pack_count;

        bool keep_going;

        bool no_send;

        static Coms* m_pInstance;

        std::vector<std::string> log_packs;
        std::vector<std::string> alarm_msgs;
};


#endif // COMS_H_INCLUDED
