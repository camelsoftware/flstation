/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "coms.h"


Coms* Coms::m_pInstance = NULL;


Coms::Coms()
{
    keep_going = false;
    enable_stream(false);
    sock = 0;

    home_lat = home_lng = home_alt = 0;

    set_v_stream_ptr(Firelink::VS_INST, (uint8_t*)&inst);
    set_v_stream_ptr(Firelink::VS_SYSTEMS, (uint8_t*)&systems);
    set_v_stream_ptr(Firelink::VS_AP_STATUS, (uint8_t*)&ap_status);
    set_v_stream_ptr(Firelink::VS_RAW_MARG, (uint8_t*)&marg);
    set_v_stream_ptr(Firelink::VS_RAW_PWM, (uint8_t*)&pwm);

    memset(&inst, 0, sizeof(inst));
    memset(&systems, 0, sizeof(systems));
    memset(&ap_status, 0, sizeof(ap_status));

    pack_count = last_pack_count = 0;


}

Coms::~Coms()
{

}

Coms* Coms::instance()
{
    if (!m_pInstance)   // Only allow one instance of class to be generated.
        m_pInstance = new Coms;

    return m_pInstance;
}

bool Coms::get_autothrottle_on()
{
    return read_bit(systems.switches, Firelink::SYSTEM_SWITCH_AUTOTHROTTLE+1);
}

bool Coms::get_autolaunch_on()
{
    return read_bit(systems.switches, Firelink::SYSTEM_SWITCH_AUTOLAUNCH+1);
}

bool Coms::get_motor_armed()
{
    return read_bit(systems.switches, Firelink::SYSTEM_SWITCH_MOTOR+1);
}

bool Coms::get_has_rc_signal()
{
    return !read_bit(systems.switches, Firelink::SYSTEM_SWITCH_NO_SIGNAL+1);
}

bool Coms::get_is_inside_geofence()
{
    return read_bit(systems.switches, Firelink::SYSTEM_SWITCH_HIT_BOUNDARY+1);
}

void Coms::lock()
{
    mute.Lock();
}

void Coms::unlock()
{
    mute.Unlock();
}

int Coms::connection_strength()
{
    if(!keep_going)
        return 0;

    return last_pack_count;
}

bool Coms::running()
{
    return keep_going;
}


bool Coms::start()
{
    //don't try to start thread if it is already running
    if(keep_going)
        return false;

    clear_in_queue();
    clear_queue();

    if(m == COMS_SERIAL)
    {
        sp.setPort(address);
        sp.setBaudrate(baudport);

        serial::Timeout st = serial::Timeout::simpleTimeout(25);
        st.write_timeout_constant = 10;

        sp.setTimeout(st);
        try
        {
            sp.open();
        }
        catch(...)
        {
            cout << "exception caught" << endl;
            return false;
        }
        if(!sp.isOpen())
            return false;
    }

    if(m == COMS_UDP)
    {
        if(sock != 0)
        {
            cout << "socket != 0" << endl;
            return false;
        }

        sock = new UDPSocket();

        if(sock->listen(5665) == -1)
        {
            cout << "socket listen failed." << endl;
            return false;
        }

        if(!sock->set_timeout(30000))
            return false;

        if(!sock->set_write_address(address.c_str(), baudport))
            return false;
    }

    Run(); //start thread
    return true;
}

void Coms::stop()
{
    if(keep_going)
    {
        keep_going = false;
        Join();
    }
}

bool Coms::wait_for_dispatch(uint16_t pid)
{
    lock();
    clear_in_queue(); //clear in queue so that reply is 'fresh'
    unlock();

    int tries = 0;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        lock();
        if(!on_queue(pid))
        {
            unlock();
            return true;
        }

        unlock();
        tries++;
    }

    clear_queue();
    return false;
}

bool Coms::wait_for_message(uint16_t pid)
{
    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        lock();
        oq = on_in_queue(pid);
        unlock();
        if(oq)
            break;
        tries++;
    }
    return oq;
}


void Coms::push_log_pack(std::string pk)
{
    if((m == COMS_LOG_PLAYBACK) && (running()))
    {
        lock();
        log_packs.push_back(pk);
        unlock();
    }
}

void Coms::ThreadFunc()
{
    keep_going = true;
    lock();


    time_t pps_time = time(NULL);
    while(keep_going)
    {
        if(difftime(time(NULL), pps_time) >= 1)
        {
            last_pack_count = pack_count;
            pack_count = 0;
            pps_time = time(NULL);
        }

        if((queue_size() == 0) && (!stream_enabled()) && (m != COMS_LOG_PLAYBACK))
        {
            if(difftime(time(NULL), keep_alive_time) >= 1)
            {
                set_keep_alive(keep_alive_seconds);
                keep_alive_time = time(NULL);
            }
        }

        if(m == COMS_UDP)
        {
            udp();

            unlock();
            sleep_ms(1);
            lock();
        }
        if(m == COMS_SERIAL)
        {
            serial();

            unlock();
            sleep_ms(10);
            lock();
        }
        if(m == COMS_LOG_PLAYBACK)
        {
            while(log_packs.size())
            {
                if(read_pack((unsigned char*)log_packs[0].c_str()) != -1)
                    pack_count++;
                else
                    cout << "bad packet" << endl;
                log_packs.erase(log_packs.begin());
            }
            sleep_ms(10);
        }

        remove_pack(Firelink::SM_SET_KEEP_ALIVE);

        unlock();
        sleep_ms(10);
        lock();
    }

    if(m == COMS_SERIAL)
        sp.close();
    if(m == COMS_UDP)
    {
        delete sock;
        sock = 0;
    }
    unlock();
}


bool Coms::serial()
{
    try
    {
        unsigned char buf[60];

        if(next_pack(buf) >= 0)
        {
            sp.write(buf, Firelink::PACKET_SIZE);
        }

        bool ret = false;
        int i = 0;
        std::string inmsg;
        while(sp.available())
        {
            unsigned char inc = sp.read(1)[0];
            i++;
            inmsg += inc;
            int eret = encode(inc);
            if(eret != -1)
            {
                if(logger != nullptr)
                {
                    std::string msg = std::string((const char*)&inmsg[i-Firelink::PACKET_SIZE+1], Firelink::PACKET_SIZE);
                    logger->log_message(msg);
                }
                ret = true;
                pack_count++;
            }
        }
        if(ret)
            return true;
    }
    catch(...)
    {
        cout << "Exception during coms iteration. Radio unplugged?" << endl;
    }

    return false;
}

bool Coms::udp()
{
    unsigned char buf[20];

    if((stream_enabled()) || (queue_size() > 0))
    {
        next_pack(buf);
        if(sock->write((char*)buf, 20) == -1)
        {
            cout << "bad socket write" << endl;
            return false;
        }
    }

    memset(buf, '\0', 20);
    sock->read(buf, 20);

    if(read_pack(buf))
    {
        if(logger != nullptr)
        {
            std::string msg = std::string((const char*)buf, 20);
            logger->log_message(msg);
        }
        pack_count++;
        return true;
    }
    return false;
}

bool Coms::on_recv_satellites(uint8_t* data)
{
    Firelink::m_satellites* ms = (Firelink::m_satellites*)data;
    if(ms->sat_num > 11)
        return true;
    sat_reports[ms->sat_num].prn = ms->prn;
    sat_reports[ms->sat_num].ss = ms->ss;
    sat_reports[ms->sat_num].azimuth = ms->azimuth;
    sat_reports[ms->sat_num].elevation = ms->elevation;
    return true;
}

bool Coms::on_recv_home_coordinates(uint8_t* data)
{
    Firelink::m_home_coordinates* hc = (Firelink::m_home_coordinates*)data;
    home_lat = hc->lat/1000000.0;
    home_lng = hc->lng/1000000.0;
    home_alt = hc->alt;
    return true;
}

bool Coms::on_recv_alarms(uint8_t* data)
{
    Firelink::m_alarms* ma = (Firelink::m_alarms*)data;

    switch(ma->alarm)
    {
    case Firelink::ALARM_LOW_BAT:
    {
        std::string msg_txt = "Low battery!";
        alarm_msgs.push_back(msg_txt);
    }
    break;
    case Firelink::ALARM_BOUNDARY:
    {
        std::string msg_txt = "Geofence has been hit!";
        alarm_msgs.push_back(msg_txt);
    }
    break;
    case Firelink::ALARM_NO_GPS:
    {
        std::string msg_txt = "No GPS position fix!";
        alarm_msgs.push_back(msg_txt);
    }
    break;
    case Firelink::ALARM_NO_SETTINGS:
    {
        std::string msg_txt = "Could not load settings!";
        alarm_msgs.push_back(msg_txt);
    }
    break;
    case Firelink::ALARM_NO_RC:
    {
        alarm_msgs.push_back("Remote control disconnected.");
    }
    break;
    case Firelink::ALARM_NO_DATALINK:
    {
        alarm_msgs.push_back("Datalink lost.");
    }
    break;
    }
    return true;
}

int Coms::get_n_alarms()
{
    return alarm_msgs.size();
}

std::string Coms::get_next_alarm()
{
    std::string ret;
    if(get_n_alarms())
    {
        ret = alarm_msgs[0];
        alarm_msgs.erase(alarm_msgs.begin());
    }
    return ret;
}

bool Coms::on_recv_n_points(uint8_t* data)
{
    Firelink::m_n_points* np = (Firelink::m_n_points*)data;
    plan_points.resize(np->wp);
    fence_points.resize(np->gp);
    return false;
}

bool Coms::on_recv_point(uint8_t* data)
{
    Firelink::m_point* mp = (Firelink::m_point*)data;
    if(mp->type == Firelink::POINT_TYPE_WAYPOINT)
    {
        if(mp->n >= plan_points.size())
            return true;
        waypoint wp;
        wp.alt = mp->alt;
        wp.pos.lat = mp->lat/1000000.0;
        wp.pos.lng = mp->lng/1000000.0;
        plan_points[mp->n] = wp;
    }
    if(mp->type == Firelink::POINT_TYPE_GEOPOINT)
    {
        if(mp->n >= fence_points.size())
            return true;

        waypoint wp;
        wp.alt = mp->alt;
        wp.pos.lat = mp->lat/1000000.0;
        wp.pos.lng = mp->lng/1000000.0;
        fence_points[mp->n] = wp;
    }
    return false;
}

bool Coms::on_recv_debug_msg(uint8_t* data)
{
    char* msg = (char*)data;
    cout << "Debug message: " << msg << endl;
    return true;
}

int Coms::get_pwm_inputs(int c)
{
    return pwm.channels[c];
}
