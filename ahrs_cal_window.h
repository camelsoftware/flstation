/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef AHRS_CAL_WINDOW_H_INCLUDED
#define AHRS_CAL_WINDOW_H_INCLUDED


#include <gtkmm.h>
#include <vector> //std::vector
#include <array>
#include "coms.h"


class MagDa : public Gtk::DrawingArea
{
public:
    enum AXIS_TYPE
    {
        AXIS_XY,
        AXIS_XZ,
        AXIS_YZ
    };

    MagDa(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    virtual ~MagDa();

    void redraw(AXIS_TYPE axt);

protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

    AXIS_TYPE axt;
};


class AccDa : public Gtk::DrawingArea
{
public:
    AccDa(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    virtual ~AccDa();

    void redraw(double x, double y, double z);
protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

    double acc_x, acc_y, acc_z;

};


class AhrsCal : public Gtk::Window
{
public:
    AhrsCal(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    virtual ~AhrsCal()
    {
        return;
    }

    void show();
    bool on_delete_event(GdkEventAny* ent);

    void timeout();

    bool got_new_cal_data();


    Gtk::Entry* mag_gain_x_entry;
    Gtk::Entry* mag_gain_y_entry;
    Gtk::Entry* mag_gain_z_entry;
    Gtk::Entry* mag_bias_x_entry;
    Gtk::Entry* mag_bias_y_entry;
    Gtk::Entry* mag_bias_z_entry;

    Gtk::Entry* accel_bias_x_entry;
    Gtk::Entry* accel_bias_y_entry;
    Gtk::Entry* accel_bias_z_entry;

protected:

    void on_cull_spin_changed();
    void on_calibrate();
    void on_reset_clicked();
    void on_ok_clicked();

    bool new_cal_done;

    Glib::RefPtr<Gtk::Builder> refBuilder;

    Gtk::ToggleButton* sample_mag_toggle;

    Gtk::SpinButton* cull_spin;

    Gtk::Label* total_samp_lbl;
    Gtk::Label* used_samp_lbl;

    Gtk::Button* calibrate_button;
    Gtk::Button* reset_button;

    MagDa* graph_xy;
    MagDa* graph_xz;
    MagDa* graph_yz;
    AccDa* accel_da;

    Gtk::Button* ok_button;
    Gtk::Button* cancel_button;
};



#endif

