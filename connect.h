/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CONNECT_H_INCLUDED
#define CONNECT_H_INCLUDED

#include <iostream>
#include <gtkmm.h>
#include "utility.h"

using namespace std;

class connectDlg : public Gtk::Dialog
{
public:
    connectDlg();
	~connectDlg();

    int baud;
    std::string coms_port;

    int page;

    int udp_port;
    std::string udp_addr;

private:

    void ok_button_clicked();
    void cancel_button_clicked();

    Gtk::Notebook* nb;
    Gtk::Entry* coms_port_entry;
    Gtk::Entry* coms_baud_entry;

    Gtk::Entry* udp_address_entry;
    Gtk::Entry* udp_port_entry;

    Gtk::Button* ok_button;
    Gtk::Button* cancel_button;
};



#endif // CONNECT_H_INCLUDED
