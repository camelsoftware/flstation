/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "configuration.h"
#include "teensy_load.h"

#include <bitset>


Config::Config(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    //ahrs cal stuff
    refBuilder->get_widget_derived("ahrs_cal_window", ahrs_cal);

    //control setup
    refBuilder->get_widget_derived("control_setup", control_setup);
    control_setup->set_transient_for(*this);

    refBuilder->get_widget("accel_bias_x", accel_bias_x);
    refBuilder->get_widget("accel_bias_y", accel_bias_y);
    refBuilder->get_widget("accel_bias_z", accel_bias_z);

    refBuilder->get_widget("mag_bias_x", mag_bias_x);
    refBuilder->get_widget("mag_bias_y", mag_bias_y);
    refBuilder->get_widget("mag_bias_z", mag_bias_z);

    refBuilder->get_widget("mag_gain_x", mag_gain_x);
    refBuilder->get_widget("mag_gain_y", mag_gain_y);
    refBuilder->get_widget("mag_gain_z", mag_gain_z);

    refBuilder->get_widget("begin_cal_button", begin_cal_button);
    refBuilder->get_widget("download_ahrs_cal_button", download_ahrs_cal_btn);
    refBuilder->get_widget("upload_ahrs_cal_button", upload_ahrs_cal_btn);

    begin_cal_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_begin_ahrs_cal_clicked));
    download_ahrs_cal_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_download_ahrs_cal_clicked));
    upload_ahrs_cal_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_ahrs_cal_clicked));


    //Control config page
    refBuilder->get_widget("servo_rc_signal_scale_spinbutton", rc_signal_scale);

    rc_signal_scale->set_digits(2);
    rc_signal_scale->set_range(0, 3.0);
    rc_signal_scale->set_value(1.0);
    rc_signal_scale->set_increments(0.05, 1.0);


    refBuilder->get_widget("aileron_min_scale", aileron_min_scale);
    aileron_min_scale->set_digits(1);
    aileron_min_scale->set_adjustment(Gtk::Adjustment::create(-70, -127, 0, 1, 10, 0));
    refBuilder->get_widget("aileron_max_scale", aileron_max_scale);
    aileron_max_scale->set_digits(1);
    aileron_max_scale->set_adjustment(Gtk::Adjustment::create(70, 0, 127, 1, 10, 0));
    refBuilder->get_widget("elevator_min_scale", elevator_min_scale);
    elevator_min_scale->set_digits(1);
    elevator_min_scale->set_adjustment(Gtk::Adjustment::create(-70, -127, 0, 1, 10, 0));
    refBuilder->get_widget("elevator_max_scale", elevator_max_scale);
    elevator_max_scale->set_digits(1);
    elevator_max_scale->set_adjustment(Gtk::Adjustment::create(70, 0, 127, 1, 10, 0));
    refBuilder->get_widget("rudder_min_scale", rudder_min_scale);
    rudder_min_scale->set_digits(1);
    rudder_min_scale->set_adjustment(Gtk::Adjustment::create(-70, -127, 0, 1, 10, 0));
    refBuilder->get_widget("rudder_max_scale", rudder_max_scale);
    rudder_max_scale->set_digits(1);
    rudder_max_scale->set_adjustment(Gtk::Adjustment::create(70, 0, 127, 1, 10, 0));
    refBuilder->get_widget("throttle_min_scale", throttle_min_scale);
    throttle_min_scale->set_digits(1);
    throttle_min_scale->set_adjustment(Gtk::Adjustment::create(-70, -127, 0, 1, 10, 0));
    refBuilder->get_widget("throttle_max_scale", throttle_max_scale);
    throttle_max_scale->set_digits(1);
    throttle_max_scale->set_adjustment(Gtk::Adjustment::create(70, 0, 127, 1, 10, 0));

    refBuilder->get_widget("ail_pwm_rev", ail_pwm_rev);
    refBuilder->get_widget("ail_ap_rev", ail_ap_rev);
    refBuilder->get_widget("ail_servo_rev", ail_servo_rev);
    refBuilder->get_widget("ele_pwm_rev", ele_pwm_rev);
    refBuilder->get_widget("ele_ap_rev", ele_ap_rev);
    refBuilder->get_widget("ele_servo_rev", ele_servo_rev);
    refBuilder->get_widget("rud_pwm_rev", rud_pwm_rev);
    refBuilder->get_widget("rud_ap_rev", rud_ap_rev);
    refBuilder->get_widget("rud_servo_rev", rud_servo_rev);
    refBuilder->get_widget("thr_pwm_rev", thr_pwm_rev);
    refBuilder->get_widget("thr_ap_rev", thr_ap_rev);
    refBuilder->get_widget("thr_servo_rev", thr_servo_rev);


    refBuilder->get_widget("configure_servo_button", config_servo_button);
    refBuilder->get_widget("download_servo_config_button", download_servo_config_button);
    refBuilder->get_widget("upload_servo_config_button", upload_servo_config_button);

    config_servo_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_setup_servo_clicked));
    download_servo_config_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_download_servo_config));
    upload_servo_config_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_servo_config));

    refBuilder->get_widget("mixer_config_combotext", mixer_config_combotext);
    refBuilder->get_widget("mixer_ratio_spinbutton", mixer_ratio_spinbutton);
    //mixer_config_combotext->set_active(0);
    mixer_ratio_spinbutton->set_range(0, 100);
    mixer_ratio_spinbutton->set_value(50);
    mixer_ratio_spinbutton->set_increments(1, 5);
    mixer_ratio_spinbutton->set_digits(0);

    refBuilder->get_widget("mixer_config_download_button", mixer_config_download_button);
    refBuilder->get_widget("mixer_config_upload_button", mixer_config_upload_button);
    mixer_config_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_download_mixer_config));
    mixer_config_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_mixer_config));


    refBuilder->get_widget("mode_select_channel", mode_select_channel);
    refBuilder->get_widget("mode_select_mode", mode_select_mode);
    mode_select_channel->set_active(0);
    mode_select_mode->set_active(0);

    refBuilder->get_widget("mode_select_download_button", mode_select_download_button);
    refBuilder->get_widget("mode_select_upload_button", mode_select_upload_button);

    mode_select_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_download_mode_select));
    mode_select_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_upload_mode_select));


    //autopilot stuff
    refBuilder->get_widget("pitch_pgain_spinbutton", pitch_pgain_spinbutton);
    refBuilder->get_widget("pitch_igain_spinbutton", pitch_igain_spinbutton);
    refBuilder->get_widget("pitch_dgain_spinbutton", pitch_dgain_spinbutton);

    refBuilder->get_widget("roll_pgain_spinbutton", roll_pgain_spinbutton);
    refBuilder->get_widget("roll_igain_spinbutton", roll_igain_spinbutton);
    refBuilder->get_widget("roll_dgain_spinbutton", roll_dgain_spinbutton);

    pitch_pgain_spinbutton->set_range(0, 10);
    pitch_pgain_spinbutton->set_digits(3);
    pitch_pgain_spinbutton->set_value(0.045);
    pitch_pgain_spinbutton->set_increments(0.001, 0.1);

    pitch_igain_spinbutton->set_range(0, 10);
    pitch_igain_spinbutton->set_digits(3);
    pitch_igain_spinbutton->set_value(0.001);
    pitch_igain_spinbutton->set_increments(0.001, 0.1);

    pitch_dgain_spinbutton->set_range(0, 10);
    pitch_dgain_spinbutton->set_digits(3);
    pitch_dgain_spinbutton->set_value(0.005);
    pitch_dgain_spinbutton->set_increments(0.001, 0.1);

    roll_pgain_spinbutton->set_range(0, 10);
    roll_pgain_spinbutton->set_digits(3);
    roll_pgain_spinbutton->set_value(0.015);
    roll_pgain_spinbutton->set_increments(0.001, 0.1);

    roll_igain_spinbutton->set_range(0, 10);
    roll_igain_spinbutton->set_digits(3);
    roll_igain_spinbutton->set_value(0.001);
    roll_igain_spinbutton->set_increments(0.001, 0.1);

    roll_dgain_spinbutton->set_range(0, 10);
    roll_dgain_spinbutton->set_digits(3);
    roll_dgain_spinbutton->set_value(0.004);
    roll_dgain_spinbutton->set_increments(0.001, 0.1);

    refBuilder->get_widget("pitch_and_roll_gain_download_button", pnr_gain_download_button);
    refBuilder->get_widget("pitch_and_roll_gain_upload_button", pnr_gain_upload_button);
    pnr_gain_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_pnr_gain_download));
    pnr_gain_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_pnr_gain_upload));


    refBuilder->get_widget("heading_pgain_spinbutton", heading_pgain_spinbutton);
    refBuilder->get_widget("heading_igain_spinbutton", heading_igain_spinbutton);
    refBuilder->get_widget("heading_dgain_spinbutton", heading_dgain_spinbutton);

    heading_pgain_spinbutton->set_range(0, 3.0);
    heading_pgain_spinbutton->set_digits(1);
    heading_pgain_spinbutton->set_value(1.0);
    heading_pgain_spinbutton->set_increments(0.1, 0.10);

    heading_igain_spinbutton->set_range(0, 0.5);
    heading_igain_spinbutton->set_digits(2);
    heading_igain_spinbutton->set_value(0.1);
    heading_igain_spinbutton->set_increments(0.01, 0.1);

    heading_dgain_spinbutton->set_range(0, 1.0);
    heading_dgain_spinbutton->set_digits(2);
    heading_dgain_spinbutton->set_value(0.5);
    heading_dgain_spinbutton->set_increments(0.01, 0.10);

    refBuilder->get_widget("alt_gain_spinbutton", alt_gain_spinbutton);

    alt_gain_spinbutton->set_range(0, 100);
    alt_gain_spinbutton->set_digits(1);
    alt_gain_spinbutton->set_value(2.5);
    alt_gain_spinbutton->set_increments(0.1, 1.0);

    refBuilder->get_widget("vs_pgain_spinbutton", vs_pgain_spinbutton);
    refBuilder->get_widget("vs_igain_spinbutton", vs_igain_spinbutton);
    refBuilder->get_widget("vs_dgain_spinbutton", vs_dgain_spinbutton);

    vs_pgain_spinbutton->set_range(0, 100);
    vs_pgain_spinbutton->set_digits(1);
    vs_pgain_spinbutton->set_value(2.5);
    vs_pgain_spinbutton->set_increments(0.1, 1.0);

    vs_igain_spinbutton->set_range(0, 100);
    vs_igain_spinbutton->set_digits(1);
    vs_igain_spinbutton->set_value(2.5);
    vs_igain_spinbutton->set_increments(0.1, 1.0);

    vs_dgain_spinbutton->set_range(0, 100);
    vs_dgain_spinbutton->set_digits(1);
    vs_dgain_spinbutton->set_value(2.5);
    vs_dgain_spinbutton->set_increments(0.1, 1.0);

    refBuilder->get_widget("alt_gain_download_button", hna_download_button);
    refBuilder->get_widget("alt_gain_upload_button", hna_upload_button);

    hna_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_hna_download));
    hna_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_hna_upload));

    refBuilder->get_widget("max_decent_spinbutton", max_decend_spinbutton);
    refBuilder->get_widget("max_climb_spinbutton", max_climb_spinbutton);

    max_decend_spinbutton->set_range(0, 6000);
    max_decend_spinbutton->set_digits(0);
    max_decend_spinbutton->set_value(1200);
    max_decend_spinbutton->set_increments(5, 100);

    max_climb_spinbutton->set_range(0, 6000);
    max_climb_spinbutton->set_digits(0);
    max_climb_spinbutton->set_value(800);
    max_climb_spinbutton->set_increments(5, 100);

    refBuilder->get_widget("max_vs_download_button", max_vs_download_button);
    refBuilder->get_widget("max_vs_upload_button", max_vs_upload_button);

    max_vs_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_max_vs_download));
    max_vs_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_max_vs_upload));

    refBuilder->get_widget("path_pgain_spinbutton", path_pgain_spinbutton);
    refBuilder->get_widget("path_igain_spinbutton", path_igain_spinbutton);
    refBuilder->get_widget("path_dgain_spinbutton", path_dgain_spinbutton);

    path_pgain_spinbutton->set_range(0, 100);
    path_pgain_spinbutton->set_digits(1);
    path_pgain_spinbutton->set_value(8.0);
    path_pgain_spinbutton->set_increments(0.1, 1.0);

    path_igain_spinbutton->set_range(0, 100);
    path_igain_spinbutton->set_digits(1);
    path_igain_spinbutton->set_value(0.5);
    path_igain_spinbutton->set_increments(0.1, 1.0);

    path_dgain_spinbutton->set_range(0, 100);
    path_dgain_spinbutton->set_digits(1);
    path_dgain_spinbutton->set_value(2.0);
    path_dgain_spinbutton->set_increments(0.1, 1.0);

    refBuilder->get_widget("path_gain_download_button", path_download_button);
    refBuilder->get_widget("path_gain_upload_button", path_upload_button);
    path_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_path_download));
    path_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_path_upload));

    refBuilder->get_widget("loiter_radius_spinbutton", loiter_radius_spinbutton);
    refBuilder->get_widget("loiter_correction_spinbutton", loiter_correction_spinbutton);

    loiter_radius_spinbutton->set_range(10, 10000);
    loiter_radius_spinbutton->set_digits(0);
    loiter_radius_spinbutton->set_value(100);
    loiter_radius_spinbutton->set_increments(1, 20);

    loiter_correction_spinbutton->set_range(0, 100.0);
    loiter_correction_spinbutton->set_digits(1);
    loiter_correction_spinbutton->set_value(8.0);
    loiter_correction_spinbutton->set_increments(0.1, 0.5);

    refBuilder->get_widget("download_loiter_settings_button", loiter_settings_download_button);
    refBuilder->get_widget("upload_loiter_settings_button", loiter_settings_upload_button);

    loiter_settings_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_loiter_settings_download));
    loiter_settings_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_loiter_settings_upload));


    refBuilder->get_widget("yaw_coupling_spinbutton", yaw_coupling_spinbutton);
    refBuilder->get_widget("pitch_coupling_spinbutton", pitch_coupling_spinbutton);

    yaw_coupling_spinbutton->set_range(0, 100.0);
    yaw_coupling_spinbutton->set_digits(1);
    yaw_coupling_spinbutton->set_value(25.0);
    yaw_coupling_spinbutton->set_increments(0.1, 10.0);

    pitch_coupling_spinbutton->set_range(0, 20.0);
    pitch_coupling_spinbutton->set_digits(2);
    pitch_coupling_spinbutton->set_value(1.2);
    pitch_coupling_spinbutton->set_increments(0.01, 10.0);

    refBuilder->get_widget("download_yaw_coupling_button", yc_download_btn);
    refBuilder->get_widget("upload_yaw_coupling_button", yc_upload_btn);

    yc_download_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_yc_download));
    yc_upload_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_yc_upload));

    refBuilder->get_widget("ma_pitch_down_spin", ma_pitch_down);
    refBuilder->get_widget("ma_pitch_up_spin", ma_pitch_up);
    refBuilder->get_widget("ma_roll_spin", ma_roll);

    ma_pitch_down->set_range(0, 90.0);
    ma_pitch_down->set_digits(0);
    ma_pitch_down->set_value(25.0);
    ma_pitch_down->set_increments(1, 5);

    ma_pitch_up->set_range(0, 90.0);
    ma_pitch_up->set_digits(0);
    ma_pitch_up->set_value(15.0);
    ma_pitch_up->set_increments(1, 5);

    ma_roll->set_range(0, 90.0);
    ma_roll->set_digits(0);
    ma_roll->set_value(60.0);
    ma_roll->set_increments(1, 5);

    refBuilder->get_widget("ma_download_btn", ma_download_btn);
    refBuilder->get_widget("ma_upload_btn", ma_upload_btn);

    ma_download_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_ma_download));
    ma_upload_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_ma_upload));


    refBuilder->get_widget("pitch_rate_spin", max_pitch_rate_spin);
    refBuilder->get_widget("roll_rate_spin", max_roll_rate_spin);

    max_pitch_rate_spin->set_range(0, 180);
    max_pitch_rate_spin->set_digits(2);
    max_pitch_rate_spin->set_value(40.0);
    max_pitch_rate_spin->set_increments(0.1, 1.0);

    max_roll_rate_spin->set_range(0, 180);
    max_roll_rate_spin->set_digits(2);
    max_roll_rate_spin->set_value(60.0);
    max_roll_rate_spin->set_increments(0.1, 1.0);

    refBuilder->get_widget("launch_algo_combo", launch_algo_combo);
    refBuilder->get_widget("launch_algo_notebook", launch_algo_notebook);

    launch_algo_combo->signal_changed().connect(sigc::mem_fun(*this, &Config::on_launch_algo_combo_changed));
    launch_algo_combo->set_active(0);

    refBuilder->get_widget("launch_throttle", launch_throttle);
    refBuilder->get_widget("launch_vertical_speed", launch_vert_speed);
    refBuilder->get_widget("launch_altitude", launch_alt);

    launch_throttle->set_range(0, 100.0);
    launch_throttle->set_digits(0);
    launch_throttle->set_value(100.0);
    launch_throttle->set_increments(1, 5);

    launch_vert_speed->set_range(0, 6000.0);
    launch_vert_speed->set_digits(0);
    launch_vert_speed->set_value(500.0);
    launch_vert_speed->set_increments(10, 100);

    launch_alt->set_range(0, 1000.0);
    launch_alt->set_digits(0);
    launch_alt->set_value(100.0);
    launch_alt->set_increments(5, 15);


    refBuilder->get_widget("shake_throw_g_threshold", sth_g_threshold);
    refBuilder->get_widget("shake_throw_timeout", sth_throw_timeout);
    refBuilder->get_widget("shake_throw_direction", sth_shake_direction);

    refBuilder->get_widget("bungee_g_threshold", bungee_g_threshold);
    refBuilder->get_widget("bungee_delay_ms", bungee_delay);
    refBuilder->get_widget("bungee_groundspeed_threshold", bungee_speed_threshold);

    refBuilder->get_widget("takeoff_rotation_speed", rotation_speed);

    sth_g_threshold->set_range(0, 5);
    sth_g_threshold->set_digits(1);
    sth_g_threshold->set_value(1.0);
    sth_g_threshold->set_increments(0.1, 0.5);

    sth_throw_timeout->set_range(0.1, 5.0);
    sth_throw_timeout->set_digits(0.1);
    sth_throw_timeout->set_value(2.5);
    sth_throw_timeout->set_increments(0.1, 0.5);

    bungee_g_threshold->set_range(0.0, 5.0);
    bungee_g_threshold->set_digits(1);
    bungee_g_threshold->set_value(2.0);
    bungee_g_threshold->set_increments(0.1, 0.5);

    bungee_delay->set_range(0.0, 5.0);
    bungee_delay->set_digits(2);
    bungee_delay->set_value(2.0);
    bungee_delay->set_increments(0.1, 0.5);

    bungee_speed_threshold->set_range(0, 200);
    bungee_speed_threshold->set_digits(0);
    bungee_speed_threshold->set_value(35);
    bungee_speed_threshold->set_increments(5, 10);

    rotation_speed->set_range(0, 200);
    rotation_speed->set_digits(0);
    rotation_speed->set_value(20);
    rotation_speed->set_increments(1, 10);

    refBuilder->get_widget("launch_download_button", launch_download_button);
    refBuilder->get_widget("launch_upload_button", launch_upload_button);

    launch_download_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_launch_download_clicked));
    launch_upload_button->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_launch_upload_clicked));

    //TODO complete the autolaunch settings stuff

    refBuilder->get_widget("at_p_spin", at_p_spin);
    refBuilder->get_widget("at_i_spin", at_i_spin);
    refBuilder->get_widget("at_d_spin", at_d_spin);

    refBuilder->get_widget("pitch_to_throttle_spin", at_pt_spin);
    refBuilder->get_widget("cruise_speed_spin", at_cs_spin);
    refBuilder->get_widget("cruise_power_spin", at_cp_spin);
    refBuilder->get_widget("min_thr_spin", min_thr_spin);

    at_p_spin->set_range(0, 5.0);
    at_p_spin->set_digits(2);
    at_p_spin->set_value(1.0);
    at_p_spin->set_increments(0.01, 5);

    at_i_spin->set_range(0, 5.0);
    at_i_spin->set_digits(2);
    at_i_spin->set_value(0.1);
    at_i_spin->set_increments(0.01, 5);

    at_d_spin->set_range(0, 5.0);
    at_d_spin->set_digits(2);
    at_d_spin->set_value(0.1);
    at_d_spin->set_increments(0.01, 5);

    at_pt_spin->set_range(0, 100);
    at_pt_spin->set_digits(1);
    at_pt_spin->set_value(10.0);
    at_pt_spin->set_increments(0.25, 5);

    at_cs_spin->set_range(0, 1000);
    at_cs_spin->set_digits(0);
    at_cs_spin->set_value(80);
    at_cs_spin->set_increments(1, 5);

    at_cp_spin->set_range(0, 100);
    at_cp_spin->set_digits(0);
    at_cp_spin->set_value(60);
    at_cp_spin->set_increments(1, 5);

    min_thr_spin->set_range(0, 100);
    min_thr_spin->set_digits(0);
    min_thr_spin->set_value(35);
    min_thr_spin->set_increments(1, 5);

    refBuilder->get_widget("at_download_btn", at_download_btn);
    refBuilder->get_widget("at_upload_btn", at_upload_btn);

    at_download_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_at_download));
    at_upload_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_at_upload));


    refBuilder->get_widget("config_firmware_browse", fmw_browse);
    refBuilder->get_widget("config_firmware_upload", fmw_upload);
    refBuilder->get_widget("config_firmware_default", fmw_default);
    refBuilder->get_widget("config_firmware_progress", fmw_progress_bar);
    refBuilder->get_widget("config_firmware_progress_lbl", fmw_progress_lbl);
    refBuilder->get_widget("config_firmware_path", fmw_path);

    fmw_browse->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_fmw_browse));
    fmw_upload->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_fmw_upload));
    fmw_default->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_fmw_upload_default));


    refBuilder->get_widget("geofence_alarm_combo", geofence_alarm_combo);
    refBuilder->get_widget("low_bat_alarm_combo", low_bat_alarm_combo);
    refBuilder->get_widget("no_gps_alarm_combo", no_gps_alarm_combo);
    refBuilder->get_widget("no_control_alarm_combo", no_ctrl_alarm_combo);
    refBuilder->get_widget("no_datalink_alarm_combo", no_datalink_alarm_combo);


    geofence_alarm_combo->set_active(0);
    low_bat_alarm_combo->set_active(0);
    no_gps_alarm_combo->set_active(0);
    no_ctrl_alarm_combo->set_active(0);
    no_datalink_alarm_combo->set_active(0);


    refBuilder->get_widget("download_alarms_button", alarm_download_btn);
    refBuilder->get_widget("upload_alarms_button", alarm_upload_btn);
    alarm_download_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_alarm_download));
    alarm_upload_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::on_alarm_upload));

//loading and saving to files
    refBuilder->get_widget("load_config_button", load_config_btn);
    refBuilder->get_widget("save_config_button", save_config_btn);
    load_config_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::load_config));
    save_config_btn->signal_clicked().connect(sigc::mem_fun(*this, &Config::save_config));

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);



}

Config::~Config()
{

}


void Config::on_launch_algo_combo_changed()
{
    launch_algo_notebook->set_current_page(launch_algo_combo->get_active_row_number());
}


void Config::timeout()
{
    int prg = teensy_load_progress();

    if(prg < 0)
    {
        Gtk::MessageDialog msg(*this, "Error during firmware upload!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("An error occured during firmware upload. Please make sure the autopilot is connected.");
        msg.run();
        teensy_reset();
    }
    fmw_progress_bar->set_fraction(prg/100.0);

    if(control_setup->is_visible())
        control_setup->timeout();

    if(ahrs_cal->is_visible())
        ahrs_cal->timeout();

    if(ahrs_cal->got_new_cal_data())
    {
        mag_bias_x->set_text(ahrs_cal->mag_bias_x_entry->get_text());
        mag_bias_y->set_text(ahrs_cal->mag_bias_y_entry->get_text());
        mag_bias_z->set_text(ahrs_cal->mag_bias_z_entry->get_text());

        mag_gain_x->set_text(ahrs_cal->mag_gain_x_entry->get_text());
        mag_gain_y->set_text(ahrs_cal->mag_gain_y_entry->get_text());
        mag_gain_z->set_text(ahrs_cal->mag_gain_z_entry->get_text());

        accel_bias_x->set_text(ahrs_cal->accel_bias_x_entry->get_text());
        accel_bias_y->set_text(ahrs_cal->accel_bias_y_entry->get_text());
        accel_bias_z->set_text(ahrs_cal->accel_bias_z_entry->get_text());
    }
}

//generic function to check for a working data link
bool Config::check_datalink()
{
    Coms* coms = Coms::instance();
    if(coms->connection_strength() < 3) //if link quality is less than 3
    {
        //show an error message and return false
        Gtk::MessageDialog msg(*this, "No datalink!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The datalink is either disconnected or the link quality is too low.");
        msg.run();
        return false;
    }
    //otherwise return true
    return true;
}

bool Config::dispatch_message(uint16_t pid)
{
    Coms* coms = Coms::instance();

    if(!coms->wait_for_dispatch(pid))
    {
        coms->lock();
        coms->remove_pack(pid);
        coms->unlock();

        Gtk::MessageDialog msg(*this, "Error sending request!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle has not acknowledged the request.");
        msg.run();
        return false;
    }
    return true;
}

bool Config::wait_for_message(uint16_t pid)
{
    Coms* coms = Coms::instance();
    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        coms->lock();
        oq = coms->on_in_queue(pid);
        coms->unlock();
        if(oq)
            break;
        tries++;
    }
    if(!oq)
    {
        Gtk::MessageDialog msg(*this, "Error receiving data!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not send the requested data.");
        msg.run();
        return false;
    }
    return true;
}



void Config::on_begin_ahrs_cal_clicked()
{
    ahrs_cal->show();
}

void Config::on_download_ahrs_cal_clicked()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    coms->clear_in_queue();
    uint16_t pid = coms->get_ahrs_cal();
    coms->unlock();

    if(!dispatch_message(pid))
        return;

    if(!wait_for_message(Firelink::VM_AHRS_CAL))
        return;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_AHRS_CAL);
    coms->unlock();

    Firelink::m_ahrs_cal* acp = (Firelink::m_ahrs_cal*)pack.data;
    stringstream ss;
    ss << acp->acc_bias_x/10000.0;

    accel_bias_x->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->acc_bias_y/10000.0;
    accel_bias_y->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->acc_bias_z/10000.0;
    accel_bias_z->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_bias_x;
    mag_bias_x->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_bias_y;
    mag_bias_y->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_bias_z;
    mag_bias_z->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_gain_x/10000.0;
    mag_gain_x->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_gain_y/10000.0;
    mag_gain_y->set_text(ss.str());

    ss.str("");
    ss.clear();
    ss << acp->mag_gain_z/10000.0;
    mag_gain_z->set_text(ss.str());
}

void Config::on_upload_ahrs_cal_clicked()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->set_ahrs_cal(atof(mag_bias_x->get_text().c_str()),
                                      atof(mag_bias_y->get_text().c_str()),
                                      atof(mag_bias_z->get_text().c_str()),
                                      atof(mag_gain_x->get_text().c_str())*10000,
                                      atof(mag_gain_y->get_text().c_str())*10000,
                                      atof(mag_gain_z->get_text().c_str())*10000,
                                            atof(accel_bias_x->get_text().c_str())*10000,
                                            atof(accel_bias_y->get_text().c_str())*10000,
                                            atof(accel_bias_z->get_text().c_str())*10000
                                            );
    coms->unlock();

    if(!dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Error uploading data.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not acknowledge the accelerometer bias data.");
        msg.run();
        return;
    }

    Gtk::MessageDialog msg(*this, "Upload successful.", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The AHRS cal data was uploaded successfully.");
    msg.run();
}

void Config::on_setup_servo_clicked()
{
    if(!check_datalink())
        return;

    control_setup->show();
}

void Config::on_download_servo_config()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    coms->clear_in_queue();
    uint16_t pid = coms->get_servo_config();
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }
    if(oq)
    {
        coms->lock();
        coms->remove_pack(pid);
        coms->unlock();

        Gtk::MessageDialog msg(*this, "Error sending request for servo configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle has not acknlowdged the request for servo configuration data.");
        msg.run();
        return;
    }

    tries = 0;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        coms->lock();
        oq = coms->on_in_queue(Firelink::VM_SERVO_CONFIG);
        coms->unlock();
        if(oq)
            break;
        tries++;
    }
    if(!oq)
    {
        Gtk::MessageDialog msg(*this, "Error receiving servo configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not reply with a servo configuration packet.");
        msg.run();
        return;
    }

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_SERVO_CONFIG);
    coms->unlock();

    Firelink::m_servo_config* scr = (Firelink::m_servo_config*)pack.data;
    cout << int(scr->ele_min) << endl;
    aileron_min_scale->set_value(scr->ail_min);
    aileron_max_scale->set_value(scr->ail_max);
    elevator_min_scale->set_value(scr->ele_min);
    elevator_max_scale->set_value(scr->ele_max);
    rudder_min_scale->set_value(scr->rud_min);
    rudder_max_scale->set_value(scr->rud_max);
    throttle_min_scale->set_value(scr->thr_min);
    throttle_max_scale->set_value(scr->thr_max);

    rc_signal_scale->set_value(scr->scale/100.0);

    coms->lock();
    pid = coms->get_channel_inversions();
    coms->unlock();

    if(!dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Error sending channel reversal settings!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not receive request for channel reversal settings.");
        msg.run();
        return;
    }
    if(!wait_for_message(Firelink::VM_CHANNEL_INVERSIONS))
    {
        Gtk::MessageDialog msg(*this, "Error sending channel reversal settings!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not send channel reversal settings.");
        msg.run();
        return;
    }

    uint8_t in, ap, out;
    in = ap = out = 0;
    coms->lock();
    if(coms->on_in_queue(Firelink::VM_CHANNEL_INVERSIONS))
        cout << "is on queue" << endl;
    Firelink::flp apack = coms->retrieve_from_in_queue(Firelink::VM_CHANNEL_INVERSIONS);
    coms->unlock();

    in = apack.data[0];
    ap = apack.data[1];
    out = apack.data[2];

    for(int i = 0; i < 3; i++)
    {
        bitset<8> bs = apack.data[i];
        cout << bs << endl;
    }
    cout << endl;

    ail_pwm_rev->set_active(read_bit(in, 1));
    ele_pwm_rev->set_active(read_bit(in, 2));
    thr_pwm_rev->set_active(read_bit(in, 3));
    rud_pwm_rev->set_active(read_bit(in, 4));

    ail_ap_rev->set_active(read_bit(ap, 1));
    ele_ap_rev->set_active(read_bit(ap, 2));
    thr_ap_rev->set_active(read_bit(ap, 3));
    rud_ap_rev->set_active(read_bit(ap, 4));

    ail_servo_rev->set_active(read_bit(out, 1));
    ele_servo_rev->set_active(read_bit(out, 2));
    thr_servo_rev->set_active(read_bit(out, 3));
    rud_servo_rev->set_active(read_bit(out, 4));

}

void Config::on_upload_servo_config()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->set_servo_config(rc_signal_scale->get_value()*100,
                   elevator_min_scale->get_value(), elevator_max_scale->get_value(),
                   aileron_min_scale->get_value(), aileron_max_scale->get_value(),
                   rudder_min_scale->get_value(), rudder_max_scale->get_value(),
                   throttle_min_scale->get_value(), throttle_max_scale->get_value());
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < 100)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }

    if(oq) //if packet is still on dispatch queue
    {
        coms->lock();
        coms->remove_pack(pid); //remove it
        coms->unlock();

        //and show an error message
        Gtk::MessageDialog msg(*this, "Error uploading servo configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not acknowledge receiving the servo configuration data.");
        msg.run();
    }


    uint8_t pwm_reverse, ap_reverse, servo_reverse;
    pwm_reverse = ap_reverse = servo_reverse = 0;
    pwm_reverse = set_bit(pwm_reverse, 1, ail_pwm_rev->get_active());
    pwm_reverse = set_bit(pwm_reverse, 2, ele_pwm_rev->get_active());
    pwm_reverse = set_bit(pwm_reverse, 3, thr_pwm_rev->get_active());
    pwm_reverse = set_bit(pwm_reverse, 4, rud_pwm_rev->get_active());

    ap_reverse = set_bit(ap_reverse, 1, ail_ap_rev->get_active());
    ap_reverse = set_bit(ap_reverse, 2, ele_ap_rev->get_active());
    ap_reverse = set_bit(ap_reverse, 3, thr_ap_rev->get_active());
    ap_reverse = set_bit(ap_reverse, 4, rud_ap_rev->get_active());

    servo_reverse = set_bit(servo_reverse, 1, ail_servo_rev->get_active());
    servo_reverse = set_bit(servo_reverse, 2, ele_servo_rev->get_active());
    servo_reverse = set_bit(servo_reverse, 3, thr_servo_rev->get_active());
    servo_reverse = set_bit(servo_reverse, 4, rud_servo_rev->get_active());

    bitset<8> bs = pwm_reverse;
    cout << "sent: " << bs << endl;

    coms->lock();
    pid = coms->set_channel_inversions(pwm_reverse, ap_reverse, servo_reverse);
    coms->unlock();

    if(!dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Failed to upload servo configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not receive channel reversal settings.");
        msg.run();
        return;
    }

    Gtk::MessageDialog msg(*this, "Servo configuration uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The new servo configuration data has been uploaded successfully.");
    msg.run();
}

void Config::on_download_mixer_config()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->get_mixer_config();
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }
    if(oq)
    {
        coms->lock();
        coms->remove_pack(pid);
        coms->unlock();

        Gtk::MessageDialog msg(*this, "Error sending request for mixer configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle has not acknlowdged the request for mixer configuration data.");
        msg.run();
        return;
    }

    tries = 0;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        coms->lock();
        oq = coms->on_in_queue(Firelink::VM_MIXER_CONFIG);
        coms->unlock();
        if(oq)
            break;
        tries++;
    }
    if(!oq)
    {
        Gtk::MessageDialog msg(*this, "Error receiving mixer configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not reply with mixer configuration data.");
        msg.run();
        return;
    }

    Firelink::m_mixer_config* mcr;
    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_MIXER_CONFIG);
    coms->unlock();

    mcr = (Firelink::m_mixer_config*)pack.data;

    mixer_config_combotext->set_active(mcr->config);
    mixer_ratio_spinbutton->set_value(mcr->mix_ratio);
}

void Config::on_upload_mixer_config()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->set_mixer_config(
                       Firelink::MIXER_CONFIGURATION(mixer_config_combotext->get_active_row_number()),
                       mixer_ratio_spinbutton->get_value_as_int());
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < 100)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }

    if(oq) //if packet is still on dispatch queue
    {
        coms->lock();
        coms->remove_pack(pid); //remove it
        coms->unlock();

        //and show an error message
        Gtk::MessageDialog msg(*this, "Error uploading mixer configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not acknowledge receiving the mixer configuration data.");
        msg.run();
    }
    else //otherwise it was sent OK
    {
        Gtk::MessageDialog msg(*this, "Mixer configuration uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The new mixer configuration data has been uploaded successfully.");
        msg.run();
    }
}

void Config::on_pnr_gain_download()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    while(coms->on_in_queue(Firelink::VM_PNR_PID))
        coms->remove_from_in_queue(Firelink::VM_PNR_PID);

    uint16_t pid = coms->get_pnr_pid();
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }
    if(oq)
    {
        coms->lock();
        coms->remove_pack(pid);
        coms->unlock();

        Gtk::MessageDialog msg(*this, "Error sending request for pitch gains!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle has not acknlowdged the request for pitch controller gain settings.");
        msg.run();
        return;
    }

    tries = 0;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        coms->lock();
        oq = coms->on_in_queue(Firelink::VM_PNR_PID);
        coms->unlock();
        if(oq)
            break;
        tries++;
    }
    if(!oq)
    {
        Gtk::MessageDialog msg(*this, "Error receiving pitch gains!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not send the pitch controller gain settings.");
        msg.run();
        return;
    }

    Firelink::m_pnr_pid* pp;
    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_PNR_PID);
    coms->unlock();

    pp = (Firelink::m_pnr_pid*)pack.data;

    pitch_pgain_spinbutton->set_value(pp->pitch_p/10000.0);
    pitch_igain_spinbutton->set_value(pp->pitch_i/10000.0);
    pitch_dgain_spinbutton->set_value(pp->pitch_d/10000.0);

    roll_pgain_spinbutton->set_value(pp->roll_p/10000.0);
    roll_igain_spinbutton->set_value(pp->roll_i/10000.0);
    roll_dgain_spinbutton->set_value(pp->roll_d/10000.0);
}

void Config::on_pnr_gain_upload()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->set_pnr_pid(pitch_pgain_spinbutton->get_value()*10000,
                    pitch_igain_spinbutton->get_value()*10000,
                    pitch_dgain_spinbutton->get_value()*10000,
                    roll_pgain_spinbutton->get_value()*10000,
                    roll_igain_spinbutton->get_value()*10000,
                    roll_dgain_spinbutton->get_value()*10000);
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < 100)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }

    if(oq) //if packet is still on dispatch queue
    {
        coms->lock();
        coms->remove_pack(pid); //remove it
        coms->unlock();

        //and show an error message
        Gtk::MessageDialog msg(*this, "Error uploading autopilot gain configuration!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not acknowledge receiving the autopilot gain configuration data.");
        msg.run();
        return;
    }

    Gtk::MessageDialog msg(*this, "Gains uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The new autopilot gain data has been uploaded successfully.");
    msg.run();
}


void Config::on_hna_download()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->get_hna_pid();
    coms->unlock();

    int tries = 0;
    bool oq;
    while(tries < fl_timeout)
    {
        sleep_ms(20);

        coms->lock();
        oq = coms->on_queue(pid);
        coms->unlock();
        if(!oq)
            break;

        tries++;
    }
    if(oq)
    {
        coms->lock();
        coms->remove_pack(pid);
        coms->unlock();

        Gtk::MessageDialog msg(*this, "Error sending request for heading and alt gain settings!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle has not acknlowdged the request for heading and alt gain settings.");
        msg.run();
        return;
    }

    tries = 0;
    while(tries < fl_timeout)
    {
        sleep_ms(20);
        coms->lock();
        oq = coms->on_in_queue(Firelink::VM_HNA_PID);
        coms->unlock();
        if(oq)
            break;
        tries++;
    }
    if(!oq)
    {
        Gtk::MessageDialog msg(*this, "Error receiving heading and alt gain settings!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The vehicle did not send the heading and alt gain settings.");
        msg.run();
        return;
    }

    Firelink::m_hna_pid* hap;
    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_HNA_PID);
    coms->unlock();

    hap = (Firelink::m_hna_pid*)pack.data;

    heading_pgain_spinbutton->set_value(hap->heading_p/1000.0);
    heading_igain_spinbutton->set_value(hap->heading_i/1000.0);
    heading_dgain_spinbutton->set_value(hap->heading_d/1000.0);

    vs_pgain_spinbutton->set_value(hap->vs_p/10.0);
    vs_igain_spinbutton->set_value(hap->vs_i/10.0);
    vs_dgain_spinbutton->set_value(hap->vs_d/10.0);

    alt_gain_spinbutton->set_value(hap->alt_p/100.0);
}

void Config::on_hna_upload()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->set_hna_pid(
                       heading_pgain_spinbutton->get_value()*1000,
                       heading_igain_spinbutton->get_value()*1000,
                       heading_dgain_spinbutton->get_value()*1000,
                                     vs_pgain_spinbutton->get_value()*10,
                                     vs_igain_spinbutton->get_value()*10,
                                     vs_dgain_spinbutton->get_value()*10,
                                     alt_gain_spinbutton->get_value()*100);
    coms->unlock();

    if(!dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Heading and altitude gain uploaded failed!", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The new heading and altitude gain failed to upload.");
        msg.run();
        return;
    }

    Gtk::MessageDialog msg(*this, "Heading and alt gains uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The new heading and alt controller gains have been uploaded successfully.");
    msg.run();
}


void Config::on_max_vs_download()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->get_max_vs();
    coms->unlock();

    if(!dispatch_message(pid))
        return;
    if(!wait_for_message(Firelink::VM_MAX_VS))
        return;

    Firelink::flp pack;
    Firelink::m_max_vs* pp;
    coms->lock();
    pack = coms->retrieve_from_in_queue(Firelink::VM_MAX_VS);
    coms->unlock();

    pp = reinterpret_cast<Firelink::m_max_vs*>(pack.data);
    max_climb_spinbutton->set_value(pp->climb);
    max_decend_spinbutton->set_value(pp->descend);
}

void Config::on_max_vs_upload()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->set_max_vs(
                       max_climb_spinbutton->get_value(),
                       max_decend_spinbutton->get_value());
    coms->unlock();

    if(!dispatch_message(pid))
        return;

    Gtk::MessageDialog msg(*this, "Vertical speed limits uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The new vertical speed limits have been uploaded successfully.");
    msg.run();
}

void Config::on_path_download()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->get_path_pid();
    coms->unlock();

    if(!dispatch_message(pid))
        return;
    if(!wait_for_message(Firelink::VM_PATH_PID))
        return;

    Firelink::m_path_pid* pp;
    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_PATH_PID);
    coms->unlock();
    pp = (Firelink::m_path_pid*)pp;
    path_pgain_spinbutton->set_value(pp->p/10000.0);
    path_igain_spinbutton->set_value(pp->i/10000.0);
    path_dgain_spinbutton->set_value(pp->d/10000.0);
}

void Config::on_path_upload()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->set_path_pid(
                       path_pgain_spinbutton->get_value()*10000,
                       path_igain_spinbutton->get_value()*10000,
                       path_dgain_spinbutton->get_value()*10000);
    coms->unlock();
    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Path controller settings uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The new path controller settings have been uploaded successfully.");
        msg.run();
    }
}

void Config::on_loiter_settings_download()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->get_loiter_settings();
    coms->unlock();


    if(!dispatch_message(pid))
        return;

    if(!wait_for_message(Firelink::VM_LOITER_SETTINGS))
        return;

    Firelink::m_loiter_settings* lsp;
    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_LOITER_SETTINGS);
    coms->unlock();

    lsp = (Firelink::m_loiter_settings*)pack.data;
    loiter_radius_spinbutton->set_value(lsp->radius);
    loiter_correction_spinbutton->set_value(lsp->p/100.0);
}

void Config::on_loiter_settings_upload()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->set_loiter_settings(
                       loiter_radius_spinbutton->get_value_as_int(),
                       loiter_correction_spinbutton->get_value()*100);
    coms->unlock();

    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Loiter settings uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The new loiter settings have been uploaded successfully.");
        msg.run();
    }
}

void Config::on_yc_download()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->get_turn_coord();
    coms->unlock();


    if(!dispatch_message(pid))
        return;

    if(!wait_for_message(Firelink::VM_TURN_COORD))
        return;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_TURN_COORD);
    coms->unlock();

    Firelink::m_turn_coord* p = (Firelink::m_turn_coord*)pack.data;
    yaw_coupling_spinbutton->set_value(p->yaw_coupling/100.0);
    pitch_coupling_spinbutton->set_value(p->pitch_to_bank/1000.0);
}

void Config::on_yc_upload()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->set_turn_coord(
                       yaw_coupling_spinbutton->get_value()*100,
                       pitch_coupling_spinbutton->get_value()*1000);
    coms->unlock();

    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Turn coordination settings uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The new turn coordination settings have been uploaded successfully.");
        msg.run();
    }
}


void Config::on_ma_download()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();

    coms->lock();
    uint16_t pid = coms->get_max_angles();
    coms->unlock();

    if(!dispatch_message(pid))
        return;

    if(!wait_for_message(Firelink::VM_MAX_ANGLES))
        return;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_MAX_ANGLES);
    coms->unlock();
    Firelink::m_max_angles* ma = (Firelink::m_max_angles*)pack.data;
    ma_pitch_down->set_value(ma->pitch_down/100.0);
    ma_pitch_up->set_value(ma->pitch_up/100.0);
    ma_roll->set_value(ma->roll/100.0);
    max_pitch_rate_spin->set_value(ma->pitch_rate/100.0);
    max_roll_rate_spin->set_value(ma->roll_rate/100.0);
}

void Config::on_ma_upload()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->set_max_angles(ma_pitch_up->get_value()*100,
                                        ma_pitch_down->get_value()*100,
                                        ma_roll->get_value()*100,
                                        max_pitch_rate_spin->get_value()*100,
                                        max_roll_rate_spin->get_value()*100);
    coms->unlock();

    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Max angles uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The maximum angle settings have been uploaded successfully.");
        msg.run();
    }
}

void Config::on_launch_download_clicked()
{
    /*
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->get_launch_settings();
    coms->unlock();

    if(!dispatch_message(pid))
        return;

    if(!wait_for_message(Firelink::VC_LAUNCH_SETTINGS_REPORT))
        return;

    coms->lock();
    Firelink::fl_pack pack = coms->retrieve_from_in_queue(Firelink::VC_LAUNCH_SETTINGS_REPORT);
    coms->unlock();

    Firelink::fm_launch_settings* ls = (Firelink::fm_launch_settings*)pack.data;
    launch_algo_combo->set_active((int)ls->launch_algorithm);
    launch_alt->set_value(ls->target_alt*10);
    launch_vert_speed->set_value(ls->vert_speed);
    launch_throttle->set_value(ls->throttle_setting);

    coms->lock();
    pid = coms->push_get_launch_settings2();
    coms->unlock();

    if(!wait_for_message(Firelink::VC_LAUNCH_SETTINGS_REPORT2))
        return;

    coms->lock();
    pack = coms->retrieve_from_in_queue(Firelink::VC_LAUNCH_SETTINGS_REPORT2);
    coms->unlock();

    Firelink::fm_launch_pack2* lp = (Firelink::fm_launch_pack2*)pack.data;
    sth_g_threshold->set_value(lp->g_threshhold/10.0);
    sth_shake_direction->set_active(lp->shake_direction);
    sth_throw_timeout->set_value(lp->timeout/10.0);

    bungee_g_threshold->set_value(lp->bungee_g_thresh/10.0);
    bungee_delay->set_value(lp->bungee_delay/10.0);
    bungee_speed_threshold->set_value(lp->bungee_speed_thresh);

    rotation_speed->set_value(lp->rotate_speed);
    */
}

void Config::on_launch_upload_clicked()
{
    /*
     if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->push_set_launch_settings((Firelink::AUTOLAUNCH_ALGORITHM)launch_algo_combo->get_active_row_number(),
                                                  launch_throttle->get_value_as_int(), launch_vert_speed->get_value(),
                                                  launch_alt->get_value_as_int());
    coms->unlock();

    if(!dispatch_message(pid))
        return;

    coms->lock();
    pid = coms->push_set_launch_settings2(sth_g_threshold->get_value(), sth_throw_timeout->get_value(), sth_shake_direction->get_active_row_number(),
                                          bungee_g_threshold->get_value(), bungee_delay->get_value(), bungee_speed_threshold->get_value_as_int(),
                                          rotation_speed->get_value_as_int());
    coms->unlock();

    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Launch settings uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The launch settings have been uploaded successfully.");
        msg.run();
    }
    */
}

void Config::on_download_mode_select()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->get_mode_select_settings();
    coms->unlock();

    if(!dispatch_message(pid))
        return;

    if(!wait_for_message(Firelink::VM_MODE_SELECT_SETTINGS))
        return;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_MODE_SELECT_SETTINGS);
    coms->unlock();

    Firelink::m_mode_select_settings* fms = (Firelink::m_mode_select_settings*)pack.data;
    mode_select_channel->set_active(fms->chan);
    mode_select_mode->set_active(fms->mode);
}

void Config::on_upload_mode_select()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->set_mode_select_settings(
                            (Firelink::AP_MODE)mode_select_mode->get_active_row_number(),
                             mode_select_channel->get_active_row_number());
    coms->unlock();

    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Mode select settings uploaded!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The mode select settings have been uploaded successfully.");
        msg.run();
    }
}

void Config::on_at_download()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->get_at_settings();
    coms->unlock();

    if(!dispatch_message(pid))
        return;
    if(!wait_for_message(Firelink::VM_AT_SETTINGS))
        return;

    Firelink::flp pack;
    coms->lock();
    pack = coms->retrieve_from_in_queue(Firelink::VM_AT_SETTINGS);
    coms->unlock();

    Firelink::m_at_settings* as = (Firelink::m_at_settings*)pack.data;
    at_pt_spin->set_value(as->vs_throttle/100.0);
    at_cs_spin->set_value(as->cruise_speed/100.0);
    at_cp_spin->set_value(as->cruise_power);
    min_thr_spin->set_value(as->min_throttle);
    at_p_spin->set_value(as->p/10000.0);
    at_i_spin->set_value(as->i/10000.0);
    at_d_spin->set_value(as->d/10000.0);
}

void Config::on_at_upload()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->set_at_settings(
                       at_p_spin->get_value()*10000,
                       at_i_spin->get_value()*10000,
                       at_d_spin->get_value()*10000,
                       at_pt_spin->get_value()*100,
                                         at_cs_spin->get_value()*100,
                                         at_cp_spin->get_value(),
                                         min_thr_spin->get_value());
    coms->unlock();

    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Autothrottle settings uploaded", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The autothrottle settings were uploaded OK.");
        msg.run();
    }
}

void Config::on_alarm_upload()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();

    uint16_t pid = coms->set_alarm_actions(
                        string_to_alarm_action(geofence_alarm_combo->get_active_text()),
                       string_to_alarm_action(low_bat_alarm_combo->get_active_text()),
                       string_to_alarm_action(no_gps_alarm_combo->get_active_text()),
                       string_to_alarm_action(no_ctrl_alarm_combo->get_active_text()),
                       string_to_alarm_action(no_datalink_alarm_combo->get_active_text()));

    coms->unlock();

    if(dispatch_message(pid))
    {
        Gtk::MessageDialog msg(*this, "Alarm actions uploaded", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The alarm actions were uploaded OK.");
        msg.run();
    }
}

void Config::on_alarm_download()
{
    if(!check_datalink())
        return;

    Coms* coms = Coms::instance();
    coms->lock();
    uint16_t pid = coms->get_alarm_actions();
    coms->unlock();

    if(!dispatch_message(pid))
        return;
    if(!wait_for_message(Firelink::VM_ALARM_ACTIONS))
        return;

    coms->lock();
    Firelink::flp pack = coms->retrieve_from_in_queue(Firelink::VM_ALARM_ACTIONS);
    coms->unlock();

    Firelink::m_alarm_actions* ala = (Firelink::m_alarm_actions*)pack.data;

    geofence_alarm_combo->set_active_text(alarm_action_to_string(ala->boundary));
    low_bat_alarm_combo->set_active_text(alarm_action_to_string(ala->low_bat));
    no_gps_alarm_combo->set_active_text(alarm_action_to_string(ala->no_gps));
    no_ctrl_alarm_combo->set_active_text(alarm_action_to_string(ala->lost_control));
    no_datalink_alarm_combo->set_active_text(alarm_action_to_string(ala->no_telem));
}

void Config::on_fmw_browse()
{
    Gtk::FileChooserDialog fcd(*this, "Open hex file", Gtk::FILE_CHOOSER_ACTION_OPEN, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    fmw_path->set_text(fcd.get_filename());
}

void Config::on_fmw_upload()
{
    teensy_load_start(this, fmw_path->get_text());
}

void Config::on_fmw_upload_default()
{
    std::string pth = share_uri;
    pth += "firetail.hex";
    teensy_load_start(this, pth);
}

void Config::load_config()
{
    Gtk::FileChooserDialog fcd(*this, "Open configuration file", Gtk::FILE_CHOOSER_ACTION_OPEN, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    std::ifstream infile;
    infile.open(fcd.get_filename());//open the input file
    if(!infile.is_open())
    {
        Gtk::MessageDialog msg(*this, "Failed to open file.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The file could not be opened.");
        msg.run();
        return;
    }

    stringstream ss;
    ss << infile.rdbuf();
    std::string contents = ss.str();

    Tokeniser tok(contents.c_str(), '\n');
    char nxt[255];
    while(tok.next(nxt, 255))
    {
        if(nxt[0] == '#')
            continue;

        std::vector<std::string> items;
        Tokeniser line(nxt, ' ');

        char c_item[255];
        while(line.next(c_item, 255))
        {
            std::string item(c_item);
            items.push_back(item);
        }

        if(items.size() < 2)
            continue;


        if(items[0] == "rc_signal_scale:")
        {
            rc_signal_scale->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "elevator_min:")
        {
            elevator_min_scale->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "elevator_max:")
        {
            elevator_max_scale->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "aileron_min:")
        {
            aileron_min_scale->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "aileron_max:")
        {
            aileron_max_scale->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "rudder_min:")
        {
            rudder_min_scale->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "rudder_max:")
        {
            rudder_max_scale->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "throttle_min:")
        {
            throttle_min_scale->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "throttle_max:")
        {
            throttle_max_scale->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "mixer_config:")
        {
            mixer_config_combotext->set_active(atoi(items[1].c_str()));
        }
        if(items[0] == "mixer_ratio:")
        {
            mixer_ratio_spinbutton->set_value(atoi(items[1].c_str()));
        }
        if(items[0] == "pitch_p_gain:")
        {
            pitch_pgain_spinbutton->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "pitch_i_gain:")
        {
            pitch_igain_spinbutton->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "pitch_d_gain:")
        {
            pitch_dgain_spinbutton->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "roll_p_gain:")
        {
            roll_pgain_spinbutton->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "roll_i_gain:")
        {
            roll_igain_spinbutton->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "roll_d_gain:")
        {
            roll_dgain_spinbutton->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "yaw_coupling:")
        {
            yaw_coupling_spinbutton->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "pitch_coupling:")
        {
            pitch_coupling_spinbutton->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "heading_p_gain:")
            heading_pgain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "heading_i_gain:")
            heading_igain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "heading_d_gain:")
            heading_dgain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "alt_gain:")
            alt_gain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "vs_p_gain:")
            vs_pgain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "vs_i_gain:")
            vs_igain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "vs_d_gain:")
            vs_dgain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "path_i_gain:")
            path_igain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "path_d_gain:")
            path_dgain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "path_p_gain:")
            path_pgain_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "loiter_radius:")
            loiter_radius_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "loiter_correction:")
            loiter_correction_spinbutton->set_value(atof(items[1].c_str()));

        if(items[0] == "max_pitch_up:")
            ma_pitch_up->set_value(atof(items[1].c_str()));

        if(items[0] == "max_pitch_down:")
            ma_pitch_down->set_value(atof(items[1].c_str()));

        if(items[0] == "max_bank_angle:")
            ma_roll->set_value(atof(items[1].c_str()));

        if(items[0] == "autothrottle_p:")
            at_p_spin->set_value(atof(items[1].c_str()));

        if(items[0] == "autothrottle_i:")
            at_i_spin->set_value(atof(items[1].c_str()));

        if(items[0] == "autothrottle_d:")
            at_d_spin->set_value(atof(items[1].c_str()));

        if(items[0] == "pitch_to_throttle_coupling:")
        {
            at_pt_spin->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "cruise_speed:")
        {
            at_cs_spin->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "cruise_power:")
        {
            at_cp_spin->set_value(atof(items[1].c_str()));
        }
        if(items[0] == "minimum_throttle:")
            min_thr_spin->set_value(atof(items[1].c_str()));
        if(items[0] == "geofence_alarm_action:")
        {
            geofence_alarm_combo->set_active(atoi(items[1].c_str()));
        }
        if(items[0] == "low_bat_alarm_action:")
        {
            low_bat_alarm_combo->set_active(atoi(items[1].c_str()));
        }
        if(items[0] == "no_gps_alarm_action:")
        {
            no_gps_alarm_combo->set_active(atoi(items[1].c_str()));
        }
        if(items[0] == "no_ctrl_alarm_action:")
        {
            no_ctrl_alarm_combo->set_active(atoi(items[1].c_str()));
        }
        if(items[0] == "no_datalink_alarm_action:")
            no_datalink_alarm_combo->set_active(atoi(items[1].c_str()));
        if(items[0] == "max_pitch_rate:")
            max_pitch_rate_spin->set_value(atof(items[1].c_str()));
        if(items[0] == "max_roll_rate:")
            max_roll_rate_spin->set_value(atof(items[1].c_str()));
        if(items[0] == "magnetometer_gains:")
        {
            if(items.size() > 4)
            {
                mag_gain_x->set_text(items[1]);
                mag_gain_y->set_text(items[2]);
                mag_gain_z->set_text(items[3]);
            }
        }
        if(items[0] == "magnetometer_bias:")
        {
            if(items.size() > 4)
            {
                mag_bias_x->set_text(items[1]);
                mag_bias_y->set_text(items[2]);
                mag_bias_z->set_text(items[3]);
            }
        }
        if(items[0] == "accelerometer_bias:")
        {
            if(items.size() > 4)
            {
                accel_bias_x->set_text(items[1]);
                accel_bias_y->set_text(items[2]);
                accel_bias_z->set_text(items[3]);
            }
        }

        if(items[0] == "launch_algo:")
            launch_algo_combo->set_active(atoi(items[1].c_str()));
        if(items[0] == "launch_throttle:")
            launch_throttle->set_value(atof(items[1].c_str()));
        if(items[0] == "launch_vert_speed:")
            launch_vert_speed->set_value(atof(items[1].c_str()));
        if(items[0] == "sth_g_threshold:")
            sth_g_threshold->set_value(atof(items[1].c_str()));
        if(items[0] == "sth_throw_timeout:")
            sth_throw_timeout->set_value(atof(items[1].c_str()));
        if(items[0] == "sth_shake_direction:")
            sth_shake_direction->set_active(atoi(items[1].c_str()));
        if(items[0] == "bungee_g_threshold:")
            bungee_g_threshold->set_value(atof(items[1].c_str()));
        if(items[0] == "bungee_delay:")
            bungee_delay->set_value(atof(items[1].c_str()));
        if(items[0] == "bungee_speed_threshold:")
            bungee_speed_threshold->set_value(atof(items[1].c_str()));
        if(items[0] == "rotation_speed:")
            rotation_speed->set_value(atof(items[1].c_str()));
        if(items[0] == "launch_target_alt:")
            launch_alt->set_value(atof(items[1].c_str()));

        if(items[0] == "mode_select_channel:")
            mode_select_channel->set_active(atoi(items[1].c_str()));
        if(items[0] == "mode_select_mode:")
            mode_select_mode->set_active(atoi(items[1].c_str()));
    }
}

void Config::save_config()
{
    Gtk::FileChooserDialog fcd(*this, "Save configuration file", Gtk::FILE_CHOOSER_ACTION_SAVE, "");
    fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
    fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
    if(fcd.run() != Gtk::RESPONSE_OK)
        return;

    std::stringstream sout;
    sout << "#FireTail UAV System Configuration file \n\n";

    sout << "magnetometer_gains: " << mag_gain_x->get_text()
                << " " << mag_gain_y->get_text()
                << " " << mag_gain_z->get_text() << "\n";
    sout << "magnetometer_bias: " << mag_bias_x->get_text()
                << " " << mag_bias_y->get_text()
                << " " << mag_bias_z->get_text() << "\n";
    sout << "accelerometer_bias: " << accel_bias_x->get_text()
                << " " << accel_bias_y->get_text()
                << " " << accel_bias_z->get_text() << "\n";
    sout << "rc_signal_scale: " << rc_signal_scale->get_value() << "\n";
    sout << "elevator_min: " << elevator_min_scale->get_value() << "\n";
    sout << "elevator_max: " << elevator_max_scale->get_value() << "\n";
    sout << "aileron_min: " << aileron_min_scale->get_value() << "\n";
    sout << "aileron_max: " << aileron_max_scale->get_value() << "\n";
    sout << "rudder_min: " << rudder_min_scale->get_value() << "\n";
    sout << "rudder_max: " << rudder_max_scale->get_value() << "\n";
    sout << "throttle_min: " << throttle_min_scale->get_value() << "\n";
    sout << "throttle_max: " << throttle_max_scale->get_value() << "\n";
    sout << "mixer_config: " << mixer_config_combotext->get_active_row_number() << "\n";
    sout << "mixer_ratio: " << mixer_ratio_spinbutton->get_value() << "\n";
    sout << "pitch_p_gain: " << pitch_pgain_spinbutton->get_value() << "\n";
    sout << "pitch_i_gain: " << pitch_igain_spinbutton->get_value() << "\n";
    sout << "pitch_d_gain: " << pitch_dgain_spinbutton->get_value() << "\n";
    sout << "roll_p_gain: " << roll_pgain_spinbutton->get_value() << "\n";
    sout << "roll_i_gain: " << roll_igain_spinbutton->get_value() << "\n";
    sout << "roll_d_gain: " << roll_dgain_spinbutton->get_value() << "\n";
    sout << "yaw_coupling: " << yaw_coupling_spinbutton->get_value() << "\n";
    sout << "pitch_coupling: " << pitch_coupling_spinbutton->get_value() << "\n";
    sout << "heading_p_gain: " << heading_pgain_spinbutton->get_value() << "\n";
    sout << "heading_i_gain: " << heading_igain_spinbutton->get_value() << "\n";
    sout << "heading_d_gain: " << heading_dgain_spinbutton->get_value() << "\n";
    sout << "vs_p_gain: " << vs_pgain_spinbutton->get_value() << "\n";
    sout << "vs_i_gain: " << vs_igain_spinbutton->get_value() << "\n";
    sout << "vs_d_gain: " << vs_dgain_spinbutton->get_value() << "\n";
    sout << "alt_gain: " << alt_gain_spinbutton->get_value() << "\n";
    sout << "path_p_gain: " << path_pgain_spinbutton->get_value() << "\n";
    sout << "path_i_gain: " << path_igain_spinbutton->get_value() << "\n";
    sout << "path_d_gain: " << path_dgain_spinbutton->get_value() << "\n";
    sout << "loiter_radius: " << loiter_radius_spinbutton->get_value() << "\n";
    sout << "loiter_correction: " << loiter_correction_spinbutton->get_value() << "\n";
    sout << "max_pitch_down: " << ma_pitch_down->get_value() << "\n";
    sout << "max_pitch_up: " << ma_pitch_up->get_value() << "\n";
    sout << "max_bank_angle: " << ma_roll->get_value() << "\n";
    sout << "autothrottle_p: " << at_p_spin->get_value() << "\n";
    sout << "autothrottle_i: " << at_i_spin->get_value() << "\n";
    sout << "autothrottle_d: " << at_d_spin->get_value() << "\n";
    sout << "pitch_to_throttle_coupling: " << at_pt_spin->get_value() << "\n";
    sout << "cruise_speed: " << at_cs_spin->get_value() << "\n";
    sout << "cruise_power: " << at_cp_spin->get_value() << "\n";
    sout << "minimum_throttle: " << min_thr_spin->get_value() << "\n";
    sout << "geofence_alarm_action: " << geofence_alarm_combo->get_active_row_number() << "\n";
    sout << "low_bat_alarm_action: " << low_bat_alarm_combo->get_active_row_number() << "\n";
    sout << "no_gps_alarm_action: " << no_gps_alarm_combo->get_active_row_number() << "\n";
    sout << "no_ctrl_alarm_action: " << no_ctrl_alarm_combo->get_active_row_number() << "\n";
    sout << "no_datalink_alarm_action:" << no_datalink_alarm_combo->get_active_row_number() << "\n";
    sout << "max_pitch_rate: " << max_pitch_rate_spin->get_value() << "\n";
    sout << "max_roll_rate: " << max_roll_rate_spin->get_value() << "\n";
    sout << "launch_algo: " << launch_algo_combo->get_active_row_number() << "\n";
    sout << "launch_throttle: " << launch_throttle->get_value() << "\n";
    sout << "launch_vert_speed: " << launch_vert_speed->get_value() << "\n";
    sout << "sth_g_threshold: " << sth_g_threshold->get_value() << "\n";
    sout << "sth_throw_timeout:" << sth_throw_timeout->get_value() << "\n";
    sout << "sth_shake_direction: " << sth_shake_direction->get_active_row_number() << "\n";
    sout << "bungee_g_threshold: " << bungee_g_threshold->get_value() << "\n";
    sout << "bungee_delay: " << bungee_delay->get_value() << "\n";
    sout << "bungee_speed_threshold: " << bungee_speed_threshold->get_value() << "\n";
    sout << "rotation_speed: " << rotation_speed->get_value() << "\n";
    sout << "launch_target_alt: " << launch_alt->get_value() << "\n";
    sout << "mode_select_channel: " << mode_select_channel->get_active_row_number() << "\n";
    sout << "mode_select_mode: " << mode_select_mode->get_active_row_number() << "\n";

    std::string filename = fcd.get_filename();
    std::string extension(filename, filename.length()-3, 3);
    if(extension != "cnf")
        filename += ".cnf";

    std::ofstream outfile;
    outfile.open(filename);
    if(!outfile.is_open())
    {
        Gtk::MessageDialog msg(*this, "Failed to save file.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The file could not be saved.");
        msg.run();
        return;
    }

    outfile << sout.str();
    outfile.close();

    Gtk::MessageDialog msg(*this, "File saved!", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("The configuration file has been saved successfully.");
    msg.run();
}

