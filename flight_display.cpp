/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <math.h>
#include <sstream>
#include "flight_display.h"

#include <iostream>

using namespace std;

baseCoordinatesDialog::baseCoordinatesDialog()
{
    Coms* coms = Coms::instance();

    Gtk::Label* lbl = Gtk::manage(new Gtk::Label());
    Gtk::Label* lbl1 = Gtk::manage(new Gtk::Label());
    lat_entry = Gtk::manage(new Gtk::Entry());
    lng_entry = Gtk::manage(new Gtk::Entry());

    std::stringstream ss;
    ss.precision(6);
    ss << coms->get_latitude();
    lat_entry->set_text(ss.str());

    ss.clear();
    ss.str(std::string());
    ss << coms->get_longitude();
    lng_entry->set_text(ss.str());

    lbl->set_text("Lat:");
    lbl1->set_text("Lng:");

    Gtk::HBox* box = Gtk::manage(new Gtk::HBox());

    box->pack_start(*lbl);
    box->pack_start(*lat_entry);
    box->pack_start(*lbl1);
    box->pack_start(*lng_entry);


    Gtk::Label* lbl2 = Gtk::manage(new Gtk::Label());
    lbl2->set_text("Elevation:");

    ele_entry = Gtk::manage(new Gtk::Entry());
    ss.clear();
    ss.str(std::string());
    ss << coms->get_alt();
    ele_entry->set_text(ss.str());

    Gtk::HBox* box2 = Gtk::manage(new Gtk::HBox());
    box2->pack_start(*lbl2);
    box2->pack_start(*ele_entry);

    this->get_vbox()->pack_start(*box);
    this->get_vbox()->pack_start(*box2);
    this->show_all();
    this->set_modal(true);

    std::string iconpath;
    iconpath = share_uri;
#ifdef _WIN32
    iconpath += "firetail.ico";
#else
    iconpath += "firetail.png";
#endif // _WIN32
    this->set_icon_from_file(iconpath);

    add_button("gtk-cancel", Gtk::ResponseType::RESPONSE_CANCEL);
    add_button("gtk-ok", Gtk::ResponseType::RESPONSE_OK);
}


magVarDialog::magVarDialog()
{
    Gtk::Label* lbl = Gtk::manage(new Gtk::Label());
    magvar_entry = Gtk::manage(new Gtk::Entry());

    lbl->set_text("Magnetic variation:");

    Gtk::HBox* box = Gtk::manage(new Gtk::HBox());

    box->pack_start(*lbl);
    box->pack_start(*magvar_entry);

    this->get_vbox()->pack_start(*box);
    this->show_all();
    this->set_modal(true);

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);

    add_button("gtk-cancel", Gtk::ResponseType::RESPONSE_CANCEL);
    add_button("gtk-ok", Gtk::ResponseType::RESPONSE_OK);
}

batCapDialog::batCapDialog()
{
    Gtk::Label* lbl = Gtk::manage(new Gtk::Label());
    cap_entry = Gtk::manage(new Gtk::Entry());

    lbl->set_text("Battery capacity (mAH)");

    Gtk::HBox* box = Gtk::manage(new Gtk::HBox());

    box->pack_start(*lbl);
    box->pack_start(*cap_entry);

    this->get_vbox()->pack_start(*box);

    box = Gtk::manage(new Gtk::HBox());
    lbl = Gtk::manage(new Gtk::Label());
    lbl->set_text("Percent full");
    pc_spin = Gtk::manage(new Gtk::SpinButton());

    pc_spin->set_digits(0);
    pc_spin->set_range(0, 100);
    pc_spin->set_increments(1, 1);
    pc_spin->set_value(100);

    box->pack_start(*lbl);
    box->pack_start(*pc_spin);

    this->get_vbox()->pack_start(*box);
    this->show_all();
    this->set_modal(true);

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);

    add_button("gtk-cancel", Gtk::ResponseType::RESPONSE_CANCEL);
    add_button("gtk-ok", Gtk::ResponseType::RESPONSE_OK);
}


mapTypeDialog::mapTypeDialog()
{
    map_types = new Gtk::ComboBoxText();
    map_types->append("Hybrid");
    map_types->append("Road map");
    map_types->append("Satellite");
    map_types->append("Terrain");
    map_types->set_active(0);

    this->get_vbox()->pack_start(*map_types);
    this->show_all();
    this->set_modal(true);

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);

    add_button("gtk-cancel", Gtk::ResponseType::RESPONSE_CANCEL);
    add_button("gtk-ok", Gtk::ResponseType::RESPONSE_OK);
}




FlightDisplay::FlightDisplay(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Window(cobject)
{
    refBuilder = _refBuilder;

    refBuilder->get_widget_derived("ADI", adi);
    refBuilder->get_widget_derived("HSI", hsi);
    //refBuilder->get_widget_derived("MFD", mfd);

    refBuilder->get_widget("mode_manual_button", mode_manual);
    refBuilder->get_widget("mode_trainer_button", mode_trainer);
    refBuilder->get_widget("mode_pitch_n_roll_button", mode_pnr);
    refBuilder->get_widget("mode_heading_n_alt_button", mode_hna);
    refBuilder->get_widget("mode_loiter_button", mode_loiter);
    refBuilder->get_widget("mode_rtb_button", mode_rtb);
    refBuilder->get_widget("mode_waypoints_button", mode_waypoints);

    mode_manual->signal_toggled().connect(sigc::mem_fun(*this, &FlightDisplay::manual_toggled));
    mode_trainer->signal_toggled().connect(sigc::mem_fun(*this, &FlightDisplay::trainer_toggled));
    mode_pnr->signal_toggled().connect(sigc::mem_fun(*this, &FlightDisplay::pnr_toggled));
    mode_hna->signal_toggled().connect(sigc::mem_fun(*this, &FlightDisplay::hna_toggled));
    mode_loiter->signal_toggled().connect(sigc::mem_fun(*this, &FlightDisplay::loiter_toggled));
    mode_rtb->signal_toggled().connect(sigc::mem_fun(*this, &FlightDisplay::rtb_toggled));
    mode_waypoints->signal_toggled().connect(sigc::mem_fun(*this, &FlightDisplay::waypoints_toggled));


    refBuilder->get_widget_derived("refresh_waypoints_window", rwp_window);
    rwp_window->set_transient_for(*this);
    refBuilder->get_widget_derived("satellites_window", satellites_window);
    satellites_window->set_transient_for(*this);
    refBuilder->get_widget_derived("alarms_window", alarms_window);
    alarms_window->set_transient_for(*this);
    refBuilder->get_widget_derived("sounds_window", sound_window);
    sound_window->set_transient_for(*this);
    refBuilder->get_widget_derived("steering_window", steering_window);
    steering_window->set_transient_for(*this);

    refBuilder->get_widget("motor_switch", motor_switch);
    motor_switch->property_active().signal_changed().connect(sigc::mem_fun(*this, &FlightDisplay::arm_motor_switched));

    Gtk::Button* btn;
    refBuilder->get_widget("next_waypoint_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_next_waypoint));

    refBuilder->get_widget("back_waypoint_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_back_waypoint));

    refBuilder->get_widget("restart_flight_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_restart_flight));

    refBuilder->get_widget("set_bat_cap_btn", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_set_bat_cap_clicked));

    refBuilder->get_widget("get_home_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_get_base_pos));

    refBuilder->get_widget("set_home_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_set_base_pos));

    refBuilder->get_widget("set_mag_var_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_mag_var_clicked));

    refBuilder->get_widget("map_type_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_change_map_type));

    refBuilder->get_widget("refresh_waypoints_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_refresh_waypoints));

/*
    TODO
    refBuilder->get_widget("menu_show_controls", menu_item);
    btn->signal_clicked().connect(sigc::mem_fun(steering_window, &Gtk::Window::show));
*/

    refBuilder->get_widget("logging_toggle", logging_toggle);
    logging_toggle->signal_toggled().connect(sigc::mem_fun(*this, &FlightDisplay::logging_toggled));

/*
    TODO
    refBuilder->get_widget("menu_hil", menu_item);
    menu_item->signal_activate().connect(sigc::mem_fun(*this, &FlightDisplay::on_hil_clicked));
*/

    refBuilder->get_widget("hil_mode_btn", hil_mode_button);
    hil_mode_button->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::on_hil_clicked));

    refBuilder->get_widget("view_sats_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(satellites_window, &Gtk::Window::show));

/*
    refBuilder->get_widget("menu_show_alarms", menu_item);
    menu_item->signal_activate().connect(sigc::mem_fun(alarms_window, &Gtk::Window::show));
*/

    refBuilder->get_widget("sound_opt_btn", btn);
    btn->signal_clicked().connect(sigc::mem_fun(sound_window, &Gtk::Window::show));

    refBuilder->get_widget("message_clear_button", btn);
    btn->signal_clicked().connect(sigc::mem_fun(*this, &FlightDisplay::clear_messages));

    refBuilder->get_widget("center_aircraft_toggle", lock_on_toggle);
    lock_on_toggle->set_active(true);

    refBuilder->get_widget("ground_speed_lbl", groundspeed_lbl);
    refBuilder->get_widget("alt_agl_lbl", alt_agl_lbl);
    refBuilder->get_widget("gps_lock_lbl", gps_lock_lbl);
    refBuilder->get_widget("rtb_dist_lbl", rtb_dist_lbl);
    refBuilder->get_widget("timer_lbl", timer_lbl);
    refBuilder->get_widget("temp_lbl", temp_lbl);
    refBuilder->get_widget("rc_rx_ok_lbl", rc_rx_ok_lbl);
    refBuilder->get_widget("inside_fence_lbl", inside_fence_lbl);
    refBuilder->get_widget("wp_dist_lbl", wp_dist_lbl);

    refBuilder->get_widget("power_remaining_progress", power_progress);
    refBuilder->get_widget("current_lbl", current_label);
    refBuilder->get_widget("voltage_lbl", volts_label);

    gps_lock_lbl->set_use_markup(true);
    rc_rx_ok_lbl->set_use_markup(true);
    inside_fence_lbl->set_use_markup(true);


    refBuilder->get_widget("message_treeview", msg_tree);
    refmsglist = Gtk::ListStore::create(m_Columns);
    msg_tree->set_model(refmsglist);

    msg_tree->append_column("", m_Columns.m_txt);

    std::string iconfile;
    iconfile = share_uri;
#ifdef _WIN32
    iconfile += "firetail.ico";
#else
    iconfile += "firetail.png";
#endif // _WIN32
    set_icon_from_file(iconfile);

    Coms* coms = Coms::instance();

    logger = make_shared<Logger>();
    coms->set_logger(logger);

    last_ap_waypoint = 0;
    timeoutctr = 0;

    maximize();
}

FlightDisplay::~FlightDisplay()
{

}

bool FlightDisplay::check_message_txt(std::string msg)
{
    Gtk::TreeModel::iterator iter = refmsglist->children().begin();
    while(iter != refmsglist->children().end())
    {
        Gtk::TreeModel::Row row = *iter;
        std::string t = row[m_Columns.m_txt];
        if(t == msg)
            return true;
        iter++;
    }
    return false;
}

void FlightDisplay::clear_messages()
{
    refmsglist->clear();
}

void FlightDisplay::alert(std::string msg)
{
    if(!check_message_txt(msg))
    {
        Gtk::TreeModel::Row row = *refmsglist->append();
        row[m_Columns.m_txt] = msg;
        if(!sound_window->alarm_sound.is_playing())
        {
            sound_window->alarm_sound.play();
        }
    }
}

void FlightDisplay::redraw()
{
    Coms* coms = Coms::instance();

    if(rwp_window->is_visible())
        rwp_window->timeout();

    if(satellites_window->is_visible())
        satellites_window->timeout();

    alarms_window->timeout();

    if(coms->get_motor_armed())
    {
        if((!motor_switch->get_active()) && (!coms->on_queue(Firelink::SM_SET_ENGAGE_SYSTEM)))
        {
            coms->lock();
            coms->set_engage_system(Firelink::SYSTEM_SWITCH_MOTOR, false);
            coms->unlock();
        }
    }
    else
    {
        if((motor_switch->get_active()) && (!coms->on_queue(Firelink::SM_SET_ENGAGE_SYSTEM)))
        {
            coms->lock();
            coms->set_engage_system(Firelink::SYSTEM_SWITCH_MOTOR, true);
            coms->unlock();
        }
    }

    //do a check to make sure that the UAV autopilot mode is actually the one that is selected
    //if there is already a set autopilot mode packet on the queue, don't worry about it
    if(!coms->on_queue(Firelink::SM_SET_AP_MODE))
    {
        //otherwise, compare the autopilot mode to the state of the toggle buttons
        //if something doesn't line up, push a set_ap_mode packet
        if((coms->get_ap_mode() != Firelink::AP_MODE_MANUAL)
            && (mode_manual->get_active()))
        {
            coms->lock();
            coms->set_ap_mode(Firelink::AP_MODE_MANUAL);
            coms->unlock();
        }
        if((coms->get_ap_mode() != Firelink::AP_MODE_TRAINER)
            && (mode_trainer->get_active()))
        {
            coms->lock();
            coms->set_ap_mode(Firelink::AP_MODE_TRAINER);
            coms->unlock();
        }
        if((coms->get_ap_mode() != Firelink::AP_MODE_PNR)
            && (mode_pnr->get_active()))
        {
            coms->lock();
            coms->set_ap_mode(Firelink::AP_MODE_PNR);
            coms->unlock();
        }
        if((coms->get_ap_mode() != Firelink::AP_MODE_HNA)
            && (mode_hna->get_active()))
        {
            coms->lock();
            coms->set_ap_mode(Firelink::AP_MODE_HNA);
            coms->unlock();
        }
        if((coms->get_ap_mode() != Firelink::AP_MODE_LOITER)
            && (mode_loiter->get_active()))
        {
            coms->lock();
            coms->set_ap_mode(Firelink::AP_MODE_LOITER);
            coms->unlock();
        }
        if((coms->get_ap_mode() != Firelink::AP_MODE_WAYPOINTS)
            && (mode_waypoints->get_active()))
        {
            coms->lock();
            coms->set_ap_mode(Firelink::AP_MODE_WAYPOINTS);
            coms->unlock();
        }
        if((coms->get_ap_mode() != Firelink::AP_MODE_RTB)
            && (mode_rtb->get_active()))
        {
            coms->lock();
            coms->set_ap_mode(Firelink::AP_MODE_RTB);
            coms->unlock();
        }
    }

    coms->lock();
    bool sound_alarm = false;
    int alarm_ctr = 0;

    while(coms->get_n_alarms()) //count number of alarms
    {
        alarm_ctr++;
        sound_alarm = true;
        if(alarm_ctr < 100)
            break;
    }
    if(sound_alarm)
    {
        if(!sound_window->alarm_sound.is_playing())
            sound_window->alarm_sound.play();
    }

    if(coms->running())
    {
        if(coms->connection_strength() < 5)
        {
            if(!sound_window->disconnect_tone.is_playing())
                sound_window->disconnect_tone.play();
        }
        else
            sound_window->disconnect_tone.stop();
    }
    else
        sound_window->disconnect_tone.stop();

    if(coms->get_ap_waypoint() != last_ap_waypoint)
    {
        sound_window->buzzer_sound.play();
        last_ap_waypoint = coms->get_ap_waypoint();
    }
    coms->unlock();

    adi->redraw();

    timeoutctr++;
    if(timeoutctr > 10)
    {
        timeoutctr = 0;
        hsi->redraw(lock_on_toggle->get_active());
        //mfd->redraw();
    }

    coms->lock();
    char buf[256];
    sprintf(buf, "%0.2f", coms->get_ground_speed());
    groundspeed_lbl->set_text(buf);

    sprintf(buf, "%0.0f", coms->get_alt()-coms->home_alt);
    alt_agl_lbl->set_text(buf);

    if(coms->get_gps_mode() == 1)
        gps_lock_lbl->set_markup("<span foreground=\"red\">NO LOCK</span>");
    if(coms->get_gps_mode() == 2)
        gps_lock_lbl->set_markup("2D LOCK");
    if(coms->get_gps_mode() == 3)
        gps_lock_lbl->set_markup("3D LOCK");
    if((coms->get_gps_mode() <= 0) || (coms->get_gps_mode() > 3))
        gps_lock_lbl->set_markup("<span foreground=\"red\">UNKNOWN</span>");


    sprintf(buf, "%0.2f", distance_between(
                        make_coordf(coms->get_latitude(), coms->get_longitude()),
                        make_coordf(coms->home_lat, coms->home_lng)));
    rtb_dist_lbl->set_text(buf);

    int input_seconds = coms->get_timer();
    int days = input_seconds / 60 / 60 / 24;
    int hours = (input_seconds / 60 / 60) % 24;
    int minutes = (input_seconds / 60) % 60;
    int seconds = input_seconds % 60;

    if(days)
        sprintf(buf, "%id %ih %im %is", days, hours, minutes, seconds);
    else if(!days && hours)
        sprintf(buf, "%ih %im %is", hours, minutes, seconds);
    else
        sprintf(buf, "%im %is", minutes, seconds);
    timer_lbl->set_text(buf);

    sprintf(buf, "%0.2f", coms->get_temperature());
    temp_lbl->set_text(buf);
    if(coms->get_temperature() > 60)
    {
        alert("Temp is > 60");
    }

    if(coms->get_has_rc_signal())
        rc_rx_ok_lbl->set_markup("YES");
    else
    {
        rc_rx_ok_lbl->set_markup("<span foreground=\"red\">NO</span>");
        alert("No RC signal!");
    }

    if(coms->get_is_inside_geofence())
        inside_fence_lbl->set_markup("YES");
    else
        inside_fence_lbl->set_markup("<span foreground=\"red\">NO</span>");

    coord here, there;
    here = make_coordf(coms->get_latitude(), coms->get_longitude());
    there = make_coordf(coms->get_ap_waypoint_lat(), coms->get_ap_waypoint_lng());
    sprintf(buf, "%0.2f", distance_between(here, there));
    wp_dist_lbl->set_text(buf);

    float pr = coms->get_power_remaining()/100.0;
    power_progress->set_fraction(pr);

    sprintf(buf, "%0.2fA  ", coms->get_amps());
    current_label->set_text(buf);

    sprintf(buf, "%0.2fV  ", coms->get_volts());
    volts_label->set_text(buf);

    if(coms->get_ap_mode() >= Firelink::AP_MODE_HNA)
    {
        if((coms->get_ap_alt() - coms->get_alt()) > 100) //if we're 100ft below selected altitude
        {
            if(coms->get_vert_speed() < 0) //and vertical speed is negative
            {
                if(alt_warn_counter > alt_warn_threshold)
                {
                    alert("Check altitude - altitude lost during climb");
                }
                else
                    alt_warn_counter++;
            }
            else
                alt_warn_counter = 0;
        }
        if((coms->get_ap_alt() - coms->get_alt()) < -100) //if we're 100ft above selected altitude
        {
            if(coms->get_vert_speed() > 0) //and vertical speed is positive
            {
                if(alt_warn_counter > alt_warn_threshold)
                {
                    alert("Check altitude - altitude gained during decent");
                }
                else
                    alt_warn_counter++;
            }
            else
                alt_warn_counter = 0;
        }
    }

    if(fabs(coms->get_roll()) > 75)
        alert("Bank angle!");
    if(fabs(coms->get_pitch()) > 45)
        alert("Pitch angle!");

    coms->unlock();
}


void FlightDisplay::manual_toggled()
{
    if(mode_manual->get_active())
    {
        Coms* coms = Coms::instance();

        coms->lock();
        coms->set_ap_mode(Firelink::AP_MODE_MANUAL);
        coms->unlock();
    }
}

void FlightDisplay::trainer_toggled()
{
    if(mode_trainer->get_active())
    {
        Coms* coms = Coms::instance();

        coms->lock();
        coms->set_ap_mode(Firelink::AP_MODE_TRAINER);
        coms->unlock();
    }
}


void FlightDisplay::pnr_toggled()
{
    if(mode_pnr->get_active())
    {
        Coms* coms = Coms::instance();

        coms->lock();
        coms->set_ap_mode(Firelink::AP_MODE_PNR);
        coms->unlock();
    }
}


void FlightDisplay::hna_toggled()
{
    if(mode_hna->get_active())
    {
        Coms* coms = Coms::instance();

        coms->lock();
        coms->set_ap_mode(Firelink::AP_MODE_HNA);
        coms->unlock();
    }
}

void FlightDisplay::loiter_toggled()
{
    if(mode_loiter->get_active())
    {
        Coms* coms = Coms::instance();

        coms->lock();
        coms->set_ap_mode(Firelink::AP_MODE_LOITER);
        coms->unlock();
    }
}

void FlightDisplay::rtb_toggled()
{
    if(mode_rtb->get_active())
    {
        Coms* coms = Coms::instance();

        coms->lock();
        coms->set_ap_mode(Firelink::AP_MODE_RTB);
        coms->unlock();
    }
}

void FlightDisplay::waypoints_toggled()
{
    if(mode_waypoints->get_active())
    {
        Coms* coms = Coms::instance();

        coms->lock();
        coms->set_ap_mode(Firelink::AP_MODE_WAYPOINTS);
        coms->unlock();
    }
}

void FlightDisplay::arm_motor_switched()
{
    Coms* coms = Coms::instance();
    coms->lock();

    if(motor_switch->get_active())
        coms->set_engage_system(Firelink::SYSTEM_SWITCH_MOTOR, true);
    else
        coms->set_engage_system(Firelink::SYSTEM_SWITCH_MOTOR, false);

    coms->unlock();
}

void FlightDisplay::autothrottle_switched()
{
    Coms* coms = Coms::instance();
    coms->lock();

    if(autothrottle_item->get_active())
        coms->set_engage_system(Firelink::SYSTEM_SWITCH_AUTOTHROTTLE, true);
    else
        coms->set_engage_system(Firelink::SYSTEM_SWITCH_AUTOTHROTTLE, false);

    coms->unlock();
}

void FlightDisplay::autolaunch_clicked()
{
    Coms* coms = Coms::instance();
    coms->lock();

    if(coms->get_autolaunch_on())
        coms->set_engage_system(Firelink::SYSTEM_SWITCH_AUTOLAUNCH, false);
    else
        coms->set_engage_system(Firelink::SYSTEM_SWITCH_AUTOLAUNCH, true);

    coms->unlock();
}



void FlightDisplay::logging_toggled()
{
    if(!logger->is_logging())
    {
        Gtk::FileChooserDialog fcd(*this, "Save log as", Gtk::FILE_CHOOSER_ACTION_SAVE, "");
        fcd.add_button(Gtk::StockID("gtk-cancel"), Gtk::RESPONSE_CANCEL);
        fcd.add_button(Gtk::StockID("gtk-ok"), Gtk::RESPONSE_OK);
        if(fcd.run() != Gtk::RESPONSE_OK)
            return;

        if(!logger->begin(fcd.get_filename()))
        {
            Gtk::MessageDialog msg(*this, "Error starting logger.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            msg.set_secondary_text("The file could not be created.");
            msg.run();
        }

        logging_toggle->set_label("Stop logging");
    }
    else
    {
        logger->end();
        logging_toggle->set_label("Start logging");
    }
}



void FlightDisplay::on_next_waypoint()
{
    Coms* coms = Coms::instance();
    coms->lock();
    coms->set_flight_command(Firelink::FLIGHT_COMMAND_NEXT_WAYPOINT);
    coms->unlock();
}

void FlightDisplay::on_back_waypoint()
{
    Coms* coms = Coms::instance();
    coms->lock();
    coms->set_flight_command(Firelink::FLIGHT_COMMAND_BACK_WAYPOINT);
    coms->unlock();
}

void FlightDisplay::on_restart_flight()
{
    Coms* coms = Coms::instance();
    coms->lock();
    coms->set_flight_command(Firelink::FLIGHT_COMMAND_RESTART);
    coms->unlock();
}


void FlightDisplay::on_set_bat_cap_clicked()
{
    batCapDialog bcd;
    if(bcd.run() == Gtk::RESPONSE_CANCEL)
        return;

    int mah = atoi(bcd.cap_entry->get_text().c_str());
    if(mah < 1)
    {
        Gtk::MessageDialog msg(*this, "Bad battery capacity. ", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        std::string wmsg;
        wmsg = "It seems odd that the battery has a capacity of " + bcd.cap_entry->get_text();
        wmsg += "mAH. Please make sure that you've got the correct units (milliamp hours).";
        msg.set_secondary_text(wmsg);
        msg.run();
    }

    double pc = bcd.pc_spin->get_value();
    pc /= 100.0;

    double tr_mah = mah*pc;
    Coms* coms = Coms::instance();
    coms->set_battery_mah(tr_mah*1000);
}

void FlightDisplay::show()
{
    Coms* coms = Coms::instance();
    if(coms->connection_strength() > 1)
    {
        motor_switch->set_active(coms->get_motor_armed());

        Firelink::AP_MODE apm = coms->get_ap_mode();
        switch(apm)
        {
            case Firelink::AP_MODE_MANUAL:
            {
                mode_manual->set_active(true);
            }
            break;
            case Firelink::AP_MODE_TRAINER:
            {
                mode_trainer->set_active(true);
            }
            break;
            case Firelink::AP_MODE_PNR:
            {
                mode_pnr->set_active(true);
            }
            break;
            case Firelink::AP_MODE_HNA:
            {
                mode_hna->set_active(true);
            }
            break;
            case Firelink::AP_MODE_LOITER:
            {
                mode_loiter->set_active(true);
            }
            break;
            case Firelink::AP_MODE_WAYPOINTS:
            {
                mode_waypoints->set_active(true);
            }
            break;
        }
    }
    hsi->reload();
    Gtk::Window::show();
}

bool FlightDisplay::on_delete_event(GdkEventAny* ent)
{
    sound_window->disconnect_tone.stop();
    sound_window->alarm_sound.stop();
    return false;
}

void FlightDisplay::on_get_base_pos()
{
    Coms* coms = Coms::instance();
    coms->lock();
    coms->get_home_coordinates();
    coms->unlock();
}

void FlightDisplay::on_set_base_pos()
{
    Coms* coms = Coms::instance();

    baseCoordinatesDialog bcdlg;
    if(bcdlg.run() == Gtk::RESPONSE_OK)
    {
        float lat, lng, ele;
        lat = atof(bcdlg.lat_entry->get_text().c_str());
        lng = atof(bcdlg.lng_entry->get_text().c_str());
        ele = atoi(bcdlg.ele_entry->get_text().c_str());

        coms->lock();
        coms->set_home_coordinates(lat*1000000, lng*1000000, ele);
        coms->get_home_coordinates();
        coms->unlock();
    }
}

void FlightDisplay::on_mag_var_clicked()
{
    Coms* coms = Coms::instance();
    magVarDialog mvd;
    if(mvd.run() == Gtk::RESPONSE_OK)
    {
        float mag_var;
        mag_var = atof(mvd.magvar_entry->get_text().c_str());

        coms->lock();
        coms->set_mag_var(mag_var*100);
        coms->unlock();
    }
}

void FlightDisplay::on_change_map_type()
{
    mapTypeDialog mtd;
    if(mtd.run() == Gtk::RESPONSE_OK)
    {
        int mt = mtd.map_types->get_active_row_number();
        switch(mt)
        {
        case 0:
        {
            hsi->set_map_type(OSM_GPS_MAP_SOURCE_GOOGLE_HYBRID);
        }
        break;
        case 1:
        {
            hsi->set_map_type(OSM_GPS_MAP_SOURCE_OPENSTREETMAP);
        }
        break;
        case 2:
        {
            hsi->set_map_type(OSM_GPS_MAP_SOURCE_VIRTUAL_EARTH_SATELLITE);
        }
        break;
        case 3:
        {
            hsi->set_map_type(OSM_GPS_MAP_SOURCE_MAPS_FOR_FREE);
        }
        break;
        }
    }
}

void FlightDisplay::on_refresh_waypoints()
{
    rwp_window->show();
}

void FlightDisplay::on_hil_clicked()
{
    Coms* coms = Coms::instance();
    //safety checks to make sure HIL isn't turned on in flight
    if(coms->get_ground_speed() > 2.0f)
    {
        Gtk::MessageDialog msg(*this, "Cannot enable HIL", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("The ground speed is greater than 2km/h. Switching to HIL test mode will cause your vehicle to crash.");
        msg.run();
        return;
    }
    if(coms->get_motor_armed())
    {
        Gtk::MessageDialog msg(*this, "Cannot enable HIL", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        msg.set_secondary_text("Cannot enable HIL whilst the motor/s are armed.");
        msg.run();
        return;
    }

    coms->lock();
    coms->set_test_mode(Firelink::TEST_FUNCTION_HIL_MODE);
    coms->unlock();
    Gtk::MessageDialog msg(*this, "HIL Enabled.", false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    msg.set_secondary_text("Vehicle is now in HIL test mode. Do not try to operate it until it has been reset.");
    msg.run();
}

/*
void FlightDisplay::on_clear_alerts()
{
    palert_model->clear();
}
*/

