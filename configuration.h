/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#include <fstream>
#include <gtkmm.h>
#include "coms.h"
#include "ahrs_cal_window.h"
#include "control_setup.h"
#include "imumaths.h"


class Config : public Gtk::Window
{
    public:
        Config(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
        virtual ~Config();


        void timeout();

    protected:
        AhrsCal* ahrs_cal;
        ControlSetup* control_setup;

        bool check_datalink();

        bool dispatch_message(uint16_t id);
        bool wait_for_message(uint16_t id);

        void on_offset_changed();
        void on_recal_ahrs_offset();
        void on_download_ahrs_offset();
        void on_upload_ahrs_offset();


        Glib::RefPtr<Gtk::Builder> refBuilder;

        //AHRS offset widgets
        Gtk::Entry* accel_bias_x;
        Gtk::Entry* accel_bias_y;
        Gtk::Entry* accel_bias_z;

        Gtk::Entry* mag_bias_x;
        Gtk::Entry* mag_bias_y;
        Gtk::Entry* mag_bias_z;

        Gtk::Entry* mag_gain_x;
        Gtk::Entry* mag_gain_y;
        Gtk::Entry* mag_gain_z;

        Gtk::Button* begin_cal_button;
        Gtk::Button* download_ahrs_cal_btn;
        Gtk::Button* upload_ahrs_cal_btn;

        void on_begin_ahrs_cal_clicked();
        void on_download_ahrs_cal_clicked();
        void on_upload_ahrs_cal_clicked();


        //Servo config widgets
        Gtk::SpinButton* rc_signal_scale;
        Gtk::Scale* aileron_min_scale;
        Gtk::Scale* aileron_max_scale;
        Gtk::Scale* elevator_min_scale;
        Gtk::Scale* elevator_max_scale;
        Gtk::Scale* rudder_min_scale;
        Gtk::Scale* rudder_max_scale;
        Gtk::Scale* throttle_min_scale;
        Gtk::Scale* throttle_max_scale;

        Gtk::CheckButton* ail_pwm_rev;
        Gtk::CheckButton* ail_ap_rev;
        Gtk::CheckButton* ail_servo_rev;
        Gtk::CheckButton* ele_pwm_rev;
        Gtk::CheckButton* ele_ap_rev;
        Gtk::CheckButton* ele_servo_rev;
        Gtk::CheckButton* rud_pwm_rev;
        Gtk::CheckButton* rud_ap_rev;
        Gtk::CheckButton* rud_servo_rev;
        Gtk::CheckButton* thr_pwm_rev;
        Gtk::CheckButton* thr_ap_rev;
        Gtk::CheckButton* thr_servo_rev;


        Gtk::Button* config_servo_button;
        Gtk::Button* upload_servo_config_button;
        Gtk::Button* download_servo_config_button;

        void on_setup_servo_clicked();
        void on_download_servo_config();
        void on_upload_servo_config();

        Gtk::ComboBoxText* mixer_config_combotext;
        Gtk::SpinButton* mixer_ratio_spinbutton;
        Gtk::Button* mixer_config_download_button;
        Gtk::Button* mixer_config_upload_button;

        void on_download_mixer_config();
        void on_upload_mixer_config();

        Gtk::ComboBoxText* mode_select_channel;
        Gtk::ComboBoxText* mode_select_mode;
        Gtk::Button* mode_select_download_button;
        Gtk::Button* mode_select_upload_button;

        void on_download_mode_select();
        void on_upload_mode_select();

        //autopilot settings
        Gtk::SpinButton* pitch_pgain_spinbutton;
        Gtk::SpinButton* pitch_igain_spinbutton;
        Gtk::SpinButton* pitch_dgain_spinbutton;

        Gtk::SpinButton* roll_pgain_spinbutton;
        Gtk::SpinButton* roll_igain_spinbutton;
        Gtk::SpinButton* roll_dgain_spinbutton;

        Gtk::Button* pnr_gain_download_button;
        Gtk::Button* pnr_gain_upload_button;

        void on_pnr_gain_download();
        void on_pnr_gain_upload();

        Gtk::SpinButton* heading_pgain_spinbutton;
        Gtk::SpinButton* heading_igain_spinbutton;
        Gtk::SpinButton* heading_dgain_spinbutton;

        Gtk::SpinButton* alt_gain_spinbutton;

        Gtk::SpinButton* vs_pgain_spinbutton;
        Gtk::SpinButton* vs_igain_spinbutton;
        Gtk::SpinButton* vs_dgain_spinbutton;

        Gtk::Button* hna_upload_button;
        Gtk::Button* hna_download_button;

        void on_hna_upload();
        void on_hna_download();

        Gtk::SpinButton* max_climb_spinbutton;
        Gtk::SpinButton* max_decend_spinbutton;

        Gtk::Button* max_vs_download_button;
        Gtk::Button* max_vs_upload_button;

        void on_max_vs_upload();
        void on_max_vs_download();

        Gtk::SpinButton* path_pgain_spinbutton;
        Gtk::SpinButton* path_igain_spinbutton;
        Gtk::SpinButton* path_dgain_spinbutton;

        Gtk::Button* path_upload_button;
        Gtk::Button* path_download_button;

        void on_path_upload();
        void on_path_download();

        Gtk::SpinButton* loiter_radius_spinbutton;
        Gtk::SpinButton* loiter_correction_spinbutton;

        Gtk::Button* loiter_settings_download_button;
        Gtk::Button* loiter_settings_upload_button;

        void on_loiter_settings_upload();
        void on_loiter_settings_download();

        Gtk::SpinButton* yaw_coupling_spinbutton;
        Gtk::SpinButton* pitch_coupling_spinbutton;

        Gtk::Button* yc_download_btn;
        Gtk::Button* yc_upload_btn;

        Gtk::SpinButton* ma_pitch_down;
        Gtk::SpinButton* ma_pitch_up;
        Gtk::SpinButton* ma_roll;

        Gtk::Button* ma_upload_btn;
        Gtk::Button* ma_download_btn;

        Gtk::ComboBoxText* launch_algo_combo;
        Gtk::Notebook* launch_algo_notebook;

        Gtk::SpinButton* launch_throttle;
        Gtk::SpinButton* launch_vert_speed;
        Gtk::SpinButton* launch_alt;

        Gtk::SpinButton* sth_g_threshold;
        Gtk::SpinButton* sth_throw_timeout;
        Gtk::ComboBoxText* sth_shake_direction;

        Gtk::SpinButton* bungee_g_threshold;
        Gtk::SpinButton* bungee_delay;
        Gtk::SpinButton* bungee_speed_threshold;

        Gtk::SpinButton* rotation_speed;

        Gtk::Button* launch_download_button;
        Gtk::Button* launch_upload_button;

        Gtk::SpinButton* at_p_spin;
        Gtk::SpinButton* at_i_spin;
        Gtk::SpinButton* at_d_spin;

        Gtk::SpinButton* at_pt_spin;
        Gtk::SpinButton* at_cs_spin;
        Gtk::SpinButton* at_cp_spin;
        Gtk::SpinButton* min_thr_spin;

        Gtk::Button* at_upload_btn;
        Gtk::Button* at_download_btn;

        Gtk::SpinButton* max_pitch_rate_spin;
        Gtk::SpinButton* max_roll_rate_spin;
        Gtk::Button* download_max_rates_btn;
        Gtk::Button* upload_max_rates_btn;

        //alarms settings
        Gtk::ComboBoxText* geofence_alarm_combo;
        Gtk::ComboBoxText* low_bat_alarm_combo;
        Gtk::ComboBoxText* no_gps_alarm_combo;
        Gtk::ComboBoxText* no_ctrl_alarm_combo;
        Gtk::ComboBoxText* no_datalink_alarm_combo;
        Gtk::Button* alarm_download_btn;
        Gtk::Button* alarm_upload_btn;

        //firmware uploading
        Gtk::Button* fmw_browse;
        Gtk::Button* fmw_upload;
        Gtk::Button* fmw_default;
        Gtk::Entry* fmw_path;

        Gtk::ProgressBar* fmw_progress_bar;
        Gtk::Label* fmw_progress_lbl;


        Gtk::Button* save_config_btn;
        Gtk::Button* load_config_btn;
        void load_config();
        void save_config();


        void on_yc_upload();
        void on_yc_download();

        void on_ma_upload();
        void on_ma_download();

        void on_at_upload();
        void on_at_download();

        void on_alarm_upload();
        void on_alarm_download();

        void on_fmw_browse();
        void on_fmw_upload();
        void on_fmw_upload_default();

        void on_launch_algo_combo_changed();
        void on_launch_download_clicked();
        void on_launch_upload_clicked();
};



#endif // CONFIG_H_INCLUDED
