/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MFD_H_INCLUDED
#define MFD_H_INCLUDED


#include <gtkmm.h>
#include <sstream>
#include "coms.h"
#include "utility.h"



class MFD : public Gtk::DrawingArea
{
    public:
        MFD(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
        virtual ~MFD();

        void redraw();

    protected:

        bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
};


#endif // MFD_H_INCLUDED
