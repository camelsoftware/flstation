/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "hsi.h"

HSI::HSI(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::Box(cobject)
{
    plane_point = osm_gps_map_point_new_degrees(0, 0);
    home_point = osm_gps_map_point_new_degrees(0, 0);

    std::string hdir = Glib::get_home_dir();
    hdir += "/.flstation";

    map = (OsmGpsMap*)g_object_new (OSM_TYPE_GPS_MAP,
                                    "map-source",OSM_GPS_MAP_SOURCE_GOOGLE_HYBRID,
                                    "tile-cache",g_strdup(OSM_GPS_MAP_CACHE_AUTO),
                                    "tile-cache-base", hdir.c_str(),
                                    "proxy-uri",g_getenv("http_proxy"),
                                    NULL);

    osd = (OsmGpsMapLayer*)g_object_new (OSM_TYPE_GPS_MAP_OSD,
                                         "show-scale",TRUE,
                                         "show-coordinates",TRUE,
                                         "show-crosshair",FALSE,
                                         "show-dpad",TRUE,
                                         "show-zoom",TRUE,
                                         "show-gps-in-dpad",TRUE,
                                         "show-gps-in-zoom",FALSE,
                                         "dpad-radius", 30,
                                         NULL);
    osm_gps_map_layer_add(OSM_GPS_MAP(map), osd);
    g_object_unref(G_OBJECT(osd));

    track = osm_gps_map_track_new();
    osm_gps_map_track_add(OSM_GPS_MAP(map), track);

    gtk_box_pack_start (
        GTK_BOX(this->gobj()),
        GTK_WIDGET(map), TRUE, TRUE, 0);

    flight_plan = osm_gps_map_track_new();
    GdkRGBA color;
    color.red = 0.2;
    color.green = 0.2;
    color.blue = 1.0;
    osm_gps_map_track_set_color(flight_plan, &color);
    osm_gps_map_track_add(OSM_GPS_MAP(map), flight_plan);

    home_track = osm_gps_map_track_new();
    color.red = 1.0;
    color.green = 0.4;
    color.blue = 0.1;
    osm_gps_map_track_set_color(home_track, &color);
    osm_gps_map_track_add_point(home_track, home_point);
    osm_gps_map_track_add_point(home_track, plane_point);
    osm_gps_map_track_add(OSM_GPS_MAP(map), home_track);

    std::string plane_path = share_uri;
    plane_path += "plane.png";
    plane_image = gdk_pixbuf_new_from_file_at_size(plane_path.c_str(), 36,36,NULL);

    std::string home_path = share_uri;
    home_path += "home.png";
    gdk_home_image = gdk_pixbuf_new_from_file_at_size(home_path.c_str(), 36, 36, NULL);

    std::string marker_path = share_uri;
    marker_path += "marker.png";
    gdk_point_image = gdk_pixbuf_new_from_file_at_size(marker_path.c_str(), 64, 64, NULL);

    osm_gps_map_set_zoom(OSM_GPS_MAP(map), 15);

    gtk_widget_show(GTK_WIDGET(map));
    not_available = true;
    last_image = NULL;
    map_home_image = NULL;
    map_point_image = NULL;
}

HSI::~HSI()
{

}

void HSI::reload()
{

}


void HSI::redraw(bool lockon)
{
    Coms* coms = Coms::instance();
    if(coms->connection_strength() < 10)
    {
        if(not_available)
        {
            //TODO make it obvious the map isn't available
        }
    }
    else
    {
        if(!not_available)
        {
            //TODO
        }

        if(!map_home_image)
            map_home_image = osm_gps_map_image_add(map, coms->home_lat, coms->home_lng, gdk_home_image);

        OsmGpsMapPoint* home_point = osm_gps_map_image_get_point(map_home_image);
        if(home_point)
            osm_gps_map_point_set_degrees(home_point, coms->home_lat, coms->home_lng);


        //if the current position is not within a few meters of the last position, update the map
        if((fabs(coms->get_latitude() - old_lat) >= 0.00002)
                || (fabs(coms->get_longitude() - old_lng) >= 0.00002))
        {
            old_lat = coms->get_latitude();
            old_lng = coms->get_longitude();
            osm_gps_map_track_add_point(track, osm_gps_map_point_new_degrees(old_lat, old_lng));
        }

        OsmGpsMapPoint* hp = osm_gps_map_track_get_point(home_track, 0);
        osm_gps_map_point_set_degrees(hp, coms->home_lat, coms->home_lng);

        hp = osm_gps_map_track_get_point(home_track, 1);
        osm_gps_map_point_set_degrees(hp, coms->get_latitude(), coms->get_longitude());


        //if there is more than 1000 points on the track, remove the first point
        //this is so the HSI doesn't bog down and take forever to draw
        if(osm_gps_map_track_n_points(track) > 250)
            osm_gps_map_track_remove_point(track, 0);

        if(!last_image)
            last_image = osm_gps_map_image_add(map, old_lat, old_lng, plane_image);

        OsmGpsMapPoint* pts = osm_gps_map_image_get_point(last_image);
        osm_gps_map_point_set_degrees(pts, old_lat, old_lng);

        float pheading = coms->get_heading();
        if(pheading < 0)
            pheading += 360;
        osm_gps_map_image_set_rotation(last_image, pheading);

        if(lockon)
        {
            int zoom;
            g_object_get(map, "zoom", &zoom, NULL);
            osm_gps_map_set_center_and_zoom(map, old_lat, old_lng, zoom);
        }

        while(osm_gps_map_track_n_points(flight_plan) > coms->plan_points.size())
        {
            osm_gps_map_track_remove_point(flight_plan, 0);
        }
        while(osm_gps_map_track_n_points(flight_plan) < coms->plan_points.size())
        {
            OsmGpsMapPoint* pt = osm_gps_map_point_new_degrees(0, 0);
            osm_gps_map_track_insert_point(flight_plan, pt, 0);
        }

        GSList* plan_points = osm_gps_map_track_get_points(flight_plan);
        for(int i = 0; i < osm_gps_map_track_n_points(flight_plan); i++)
        {
            OsmGpsMapPoint* pt = (OsmGpsMapPoint*)plan_points->data;
            osm_gps_map_point_set_degrees(pt, coms->plan_points[i].pos.lat, coms->plan_points[i].pos.lng);
            plan_points = plan_points->next;
        }

        //osm_gps_map_track_add(map, flight_plan);

        if(coms->get_ap_mode() >= Firelink::AP_MODE_LOITER)
        {
            if(map_point_image == NULL)
                map_point_image = osm_gps_map_image_add(map, coms->get_ap_waypoint_lat(), coms->get_ap_waypoint_lng(), gdk_point_image);
            else
            {
                OsmGpsMapPoint* mp = osm_gps_map_image_get_point(map_point_image);
                mp->rlat = to_radians(coms->get_ap_waypoint_lat());
                mp->rlon = to_radians(coms->get_ap_waypoint_lng());
            }
        }
        else
        {
            if(map_point_image)
            {
                osm_gps_map_image_remove(map, map_point_image);
                map_point_image = NULL;
            }
        }

        osm_gps_map_map_redraw(map);
    }
}

void HSI::set_map_type(OsmGpsMapSource_t src)
{
    g_object_set(map, "map-source", src, NULL);
}


