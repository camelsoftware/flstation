/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "mfd.h"

MFD::MFD(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& _refBuilder)
    : Gtk::DrawingArea(cobject)
{
}

MFD::~MFD()
{

}


void MFD::redraw()
{
    Glib::RefPtr<Gdk::Window> win = get_window();
    if(win)
    {
        win->invalidate(false);
    }
}

bool MFD::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    Coms* coms = Coms::instance();
    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);
    cr->rectangle(0, 0, width, height);
    cr->fill();


    //draw ground speed indication
    cr->set_source_rgb(0.9, 0.2, 0.0);
    cr->set_font_size(18);
    cr->select_font_face("Monospace", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_BOLD);

    cr->move_to(20, 30);
    std::stringstream ss;
    ss.precision(2);
    ss << "GROUND SPEED    " << fixed << coms->get_ground_speed() << " KM/H";
    cr->show_text(ss.str());
    cr->fill();

    cr->set_source_rgb(0.2, 0.8, 0.0);
    cr->move_to(20, 50);
    ss.clear();
    ss.str(std::string());
    ss << "ALT AGL         " << fixed << coms->get_alt() - coms->home_alt << " FT";
    cr->show_text(ss.str());
    cr->fill();

    cr->set_source_rgb(0.8, 0.8, 0.8);
    cr->set_font_size(12);
    cr->select_font_face("Monospace", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);

    ss.clear();
    ss.str(std::string());
    ss << "GPS LOCK        ";
    cr->move_to(10, 80);
    if(coms->get_gps_mode() < 2)
        cr->set_source_rgb(1.0, 0.0, 0.0);
    if(coms->get_gps_mode() == 1)
        ss << "NO LOCK";
    if(coms->get_gps_mode() == 2)
        ss << "2D LOCK";
    if(coms->get_gps_mode() == 3)
        ss << "3D LOCK";
    if((coms->get_gps_mode() <= 0) || (coms->get_gps_mode() > 3))
        ss << "UNKNOWN";
    cr->show_text(ss.str());

    cr->set_source_rgb(0.8, 0.8, 0.8);
    cr->move_to(10, 100);
    ss.clear();
    ss.str(std::string());
    ss << "DIST            " << distance_between(
                        make_coordf(coms->get_latitude(), coms->get_longitude()),
                        make_coordf(coms->home_lat, coms->home_lng)) << " M";

    cr->show_text(ss.str());
    cr->fill();

    cr->move_to(10, 120);

    int input_seconds = coms->get_timer();
    int days = input_seconds / 60 / 60 / 24;
    int hours = (input_seconds / 60 / 60) % 24;
    int minutes = (input_seconds / 60) % 60;
    int seconds = input_seconds % 60;

    ss.clear();
    ss.str(std::string());
    ss << "TIMER           ";
    if(days)
        ss << days << "d ";
    if(days || hours)
        ss << hours << "h ";

    ss << minutes << "m " << seconds << "s";
    cr->show_text(ss.str());
    cr->fill();

    //draw temperature
    cr->move_to(10, 140);
    ss.clear();
    ss.str(std::string());
    ss << "TEMP            " << fixed << coms->get_temperature() << " C";
    cr->show_text(ss.str());
    cr->fill();


    cr->move_to(250, 80);
    ss.clear();
    ss.str(std::string());
    ss << "RC Rx OK        ";
    if(coms->get_has_rc_signal())
    {
        cr->set_source_rgb(0.8, 0.8, 0.8);
        ss << "YES";
    }
    else
    {
        cr->set_source_rgb(1.0, 0.0, 0.0);
        ss << "NO";
    }
    cr->show_text(ss.str());
    cr->fill();

    cr->move_to(250, 100);
    ss.clear();
    ss.str(std::string());
    ss << "INSIDE GEOFENCE ";
    if(coms->get_is_inside_geofence())
    {
        cr->set_source_rgb(0.8, 0.8, 0.8);
        ss << "YES";
    }
    else
    {
        cr->set_source_rgb(1.0, 0.0, 0.0);
        ss << "NO";
    }
    cr->show_text(ss.str());
    cr->fill();


    if((coms->get_ap_mode() == Firelink::AP_MODE_LOITER) || (coms->get_ap_mode() == Firelink::AP_MODE_WAYPOINTS))
    {
        cr->move_to(250, 120);
        ss.clear();
        ss.str(std::string());
        ss << "DIST TO WP\t\t";

        coord here, there;
        here = make_coordf(coms->get_latitude(), coms->get_longitude());
        there = make_coordf(coms->get_ap_waypoint_lat(), coms->get_ap_waypoint_lng());
        ss << distance_between(here, there);
        cr->show_text(ss.str());
        cr->fill();
    }


    //draw power remaining indicator
    cr->move_to(10, 180);
    cr->set_source_rgb(0.8, 0.8, 0.8);
    ss.clear();
    ss.str(std::string());
    ss << "POWER REMAINING: " << int(coms->get_power_remaining()) << "%";
    cr->show_text(ss.str());

    cr->rectangle(10, 190, width-20, 15);
    cr->stroke();

    float green_pc = coms->get_power_remaining() / 100.0f; //green percentage
    float red_pc = 1.0f - green_pc;
    cr->set_source_rgb(red_pc, green_pc, 0.0);
    int bar_width = (width-20)*green_pc;
    cr->rectangle(10, 190, bar_width, 15);
    cr->fill();

    cr->move_to(10, 230);
    cr->set_source_rgb(0.8, 0.8, 0.8);
    ss.clear();
    ss.str(std::string());
    ss.precision(2);
    ss << fixed << float(coms->get_amps()) << " A";
    cr->show_text(make_padded_string("AMPS: ", ss.str(), ' ', 20));

    cr->move_to(260, 230);
    ss.clear();
    ss.str(std::string());
    ss << fixed << float(coms->get_volts()) << " V";
    cr->show_text(make_padded_string("VOLTS: ", ss.str(), ' ', 20));



    if(coms->connection_strength() < 10)
    {
        cr->set_source_rgba(1.0, 0.0, 0.0, 1.0);
        cr->move_to(0, 0);
        cr->line_to(width, height);
        cr->stroke();

        cr->move_to(width, 0);
        cr->line_to(0, height);
        cr->stroke();
    }
    return true;
}


