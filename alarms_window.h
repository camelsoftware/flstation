/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ALARMS_WINDOW_INCLUDED
#define ALARMS_WINDOW_INCLUDED

#include <gtkmm.h>
#include "coms.h"


class AlarmsWindow : public Gtk::Window
{
public:
    AlarmsWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);
    ~AlarmsWindow();

    void timeout();

private:
    void clear();


    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:

        ModelColumns()
        {
            add(text);
        }

        Gtk::TreeModelColumn<std::string> text;
    };

    ModelColumns m_Columns;
    Gtk::TreeView* treeview;
    Glib::RefPtr<Gtk::ListStore> m_refTreeModel;

    Glib::RefPtr<Gtk::Builder> refBuilder;

    int ctr;

};




#endif // ALARMS_WINDOW_INCLUDED
