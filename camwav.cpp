/*
 *     Copyright (C) 2014  Samuel Cowen
 *
 *     Camel's Dead Simple OpenAL wrapper. Makes it dead simple to play wav files.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "camwav.h"
#include <iostream>
#include <vector>
#include <array>

using namespace camwav;


/** a bit of a hack (or is it simply C++ wizardry?) to transparently initialise & shutdown alut */
class ___camwav_alut_init
{
public:
	___camwav_alut_init()
	{
		device = alcOpenDevice(alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER));
		context = alcCreateContext(device, NULL);
		if (!alcMakeContextCurrent(context))
		{
			std::cout << "Error opening audio device." << std::endl;
		}
	}

	~___camwav_alut_init()
	{
		device = alcGetContextsDevice(context);
		alcMakeContextCurrent(NULL);
		alcDestroyContext(context);
		alcCloseDevice(device);
	}

private:
	ALCdevice *device;
	ALCcontext *context;
};

static ___camwav_alut_init __alut_init;

/** end init/exit hack */


Buffer::Buffer(std::string fname)
{
    create_from_file(fname);
}

bool Buffer::create_from_file(std::string fname)
{
    SF_INFO info;
    SNDFILE* file = sf_open(fname.c_str(), SFM_READ, &info);
    if(!file)
        return false;

    std::vector<uint16_t> data;

    std::array<int16_t, 4096> read_buf;
    size_t read_size = 0;
    while((read_size = sf_read_short(file, read_buf.data(), read_buf.size())) != 0)
    {
        data.insert(data.end(), read_buf.begin(), read_buf.begin() + read_size);
    }

    alGenBuffers(1, &buffer);
    alBufferData(buffer, info.channels == 1? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16,
            &data.front(), data.size() * sizeof(uint16_t), info.samplerate);
	error = alGetError();
	if(error == AL_NO_ERROR)
        return true;
    return false;
}

Buffer::~Buffer()
{
	alDeleteBuffers(1, &buffer);
}


Sound::Sound(Buffer* buf)
{
	set_buffer(buf);
}

Sound::~Sound()
{
	alDeleteSources(1, &source);
}

void Sound::set_buffer(Buffer* buf)
{
    alGenSources((ALuint)1, &source);
	alSourcei(source, AL_BUFFER, buf->get_al_buffer());
	error = alGetError();

	alSourcef(source, AL_PITCH, 1);
	alSourcef(source, AL_GAIN, 1);
	alSource3f(source, AL_POSITION, 0, 0, 0);
	alSource3f(source, AL_VELOCITY, 0, 0, 0);
	alSourcei(source, AL_LOOPING, AL_FALSE);
}

bool Sound::is_playing()
{
    int val;
    alGetSourcei(source, AL_SOURCE_STATE, &val);
    return (val == AL_PLAYING);
}

void Sound::play()
{
	alSourcePlay(source);
}

void Sound::stop()
{
	alSourceStop(source);
}

void Sound::pause()
{
	alSourcePause(source);
}

void Sound::set_volume(float vol)
{
    alSourcef(source, AL_GAIN, vol);
}


