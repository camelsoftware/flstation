/*
	cThread - A C++ threading library
    Copyright (C) 2012  Samuel Cowen, www.camelsoftware.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "cThread.h"

using namespace Camel;

Mutex::Mutex(const Mutex &in_mutex)
{
    init();

    if(in_mutex._locked && !_locked) Lock();
    else if(!in_mutex._locked && _locked) Unlock();
}

Mutex::~Mutex()
{
    pthread_mutex_unlock(&_mutex);
    pthread_mutex_destroy(&_mutex);
}

bool Mutex::Lock()
{
    _locked = true;
    return pthread_mutex_lock(&_mutex) == 0;
}

bool Mutex::TryLock()
{
    _locked = true;
    return pthread_mutex_trylock(&_mutex) == 0;
}

bool Mutex::Unlock()
{
    _locked = false;
    return pthread_mutex_unlock(&_mutex) == 0;
}

bool Mutex::IsLocked()
{
    return _locked;
}

void Mutex::init()
{
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr,PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&_mutex,&attr);
    pthread_mutexattr_destroy(&attr);

    _locked = false;
}


Thread::Thread()
{
    _isRunning = false;
}

Thread::~Thread()
{
    if(!_isRunning) return;
    wait();
}


bool Thread::Run()
{
    if(_isRunning) return false;
    return pthread_create(&_thread, NULL, Thread::Starter, static_cast< void* >(this)) == 0;
}

bool Thread::Join()
{
    return pthread_join(_thread, NULL) == 0;
}

bool Thread::Kill()
{
    if(!_isRunning) return false;
    _isRunning = false;
    return pthread_kill( _thread, SIGKILL) == 0;
}

