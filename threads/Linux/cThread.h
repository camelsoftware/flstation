/*
	cThread - A C++ threading library
    Copyright (C) 2012  Samuel Cowen, www.camelsoftware.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CAMEL_THREADS_H
#define CAMEL_THREADS_H

#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>


namespace Camel
{

class Mutex
{
public:
    Mutex()
    { init(); }

    Mutex(const Mutex &in_mutex);

    Mutex& operator=(const Mutex &in_mutex)
    {
        if(in_mutex._locked && !_locked) Lock();
        else if(!in_mutex._locked && _locked) Unlock();
        return *this;
    }

    ~Mutex();

    bool Lock();
    bool TryLock();
    bool Unlock();

    bool IsLocked();

private:
    pthread_mutex_t _mutex;

    bool _locked;

    void init();
};



class Thread
{
public:
    Thread();
    virtual ~Thread();

    bool Run();
    bool Join();
    bool Kill();

    bool IsRunning()
    { return _isRunning; }

    virtual void ThreadFunc(){ }

private:
    void operator=(const Thread &){}
    Thread(const Thread &){}
    pthread_t _thread;
    bool _isRunning;

    static void* Starter(void* in_thread)
    {
        Thread* thread = static_cast< Thread * >(in_thread);
        thread->_isRunning = true;
        thread->ThreadFunc();
        thread->_isRunning = false;

        return 0x00;
    }
};

};

#endif

