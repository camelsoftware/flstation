/*
	cThread - A C++ threading library
    Copyright (C) 2012  Samuel Cowen, www.camelsoftware.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "cThread.h"

using namespace Camel;

Mutex::Mutex(const Mutex &in_mutex)
{
    init();

    if(in_mutex._locked && !_locked) Lock();
    else if(!in_mutex._locked && _locked) Unlock();
}

Mutex::~Mutex()
{
    DeleteCriticalSection(&_mutex);
}

bool Mutex::Lock()
{
    _locked = true;
    EnterCriticalSection(&_mutex);
    return true;
}

bool Mutex::TryLock()
{
    _locked = true;
    return TryEnterCriticalSection(&_mutex);
}

bool Mutex::Unlock()
{
    _locked = false;
    LeaveCriticalSection(&_mutex);
    return true;
}

bool Mutex::IsLocked()
{
    return _locked;
}

void Mutex::init()
{
    InitializeCriticalSection(&_mutex);

    _locked = false;
}


Thread::Thread()
{
    _isRunning = false;
    _thread = 0x00;
}

Thread::~Thread()
{
    if(!_isRunning) return;
    CloseHandle(_thread);
}


bool Thread::Run()
{
    if(_isRunning) return false;
    _thread = CreateThread( 0x00, 0x00,Thread::Starter, static_cast< void* >(this), 0x00, 0x00);
    return _thread != NULL;
}

bool Thread::Join()
{
    return WaitForSingleObject(_thread,INFINITE) == 0x00000000L;
}

bool Thread::Kill()
{
    if(!_isRunning) return false;
    _isRunning = false;
    bool success = TerminateThread(_thread,1) && CloseHandle(_thread);
    _thread = 0x00;
    return success;
}

